/*
 * board.java
 *
 * Created on 31 ���������� 2006, 6:00 ��
 */

package gui;
import monopoly.*;
import javax.swing.*;
import java.awt.Color;

/**
 *
 * @author  soulis
 */
public class board extends javax.swing.JFrame {
    int numOfPlayers;
    GameMaster gameMaster;
    Color old;
    card cardToExecute;
    String[] names;
    boolean rentPayed = false;
    
    // Ta labels sto jPanel antistoixoun se name,rent,owner
    javax.swing.JLabel[][] CellLabels = new javax.swing.JLabel[42][4];
    // Array me ta labels katastasis twn paiktwn
    javax.swing.JLabel[] playerLabels = new javax.swing.JLabel[6];
    // Array me ta lists idioktision ton paiktwn
    javax.swing.JList[] playerProperties = new javax.swing.JList[6];
    
    // Ta defaultListModel yparxoun gia prosthafairesi antikeimenwn stis listes idioktisias twn paiktwn
    DefaultListModel model1;
    DefaultListModel model2;
    DefaultListModel model3;
    DefaultListModel model4;
    DefaultListModel model5;
    DefaultListModel model6;
    DefaultListModel listModels[] = new DefaultListModel[6];
    
    //Buydialog
    DefaultListModel model7;
    DefaultListModel model8;
    
    boolean listsEnabled[];
    
    boolean buyButtonEnabled = true;
    boolean rollDiceButtonEnabled = true;
    boolean buildHouseButtonEnabled = true;
    boolean buildHotelButtonEnabled = true;
    boolean useJailCardButtonEnabled = true;
    boolean drawCardButtonEnabled = true;
    boolean setMortButtonEnabled = true;
    boolean unMortButtonEnabled = true;
    boolean sellToPlayerButtonEnabled = true;
    boolean buyFromPlayerButtonEnabled = true;
    
    
    /** Creates new form board */
    public board(GameMaster a,int players) {
        
        int ctr;
        this.gameMaster = a;
        this.numOfPlayers = players;
        listsEnabled = new boolean[6];
        
        model1 = new DefaultListModel();
        model2 = new DefaultListModel();
        model3 = new DefaultListModel();
        model4 = new DefaultListModel();
        model5 = new DefaultListModel();
        model6 = new DefaultListModel();
        
        initComponents();
        
        //Match player labels
        playerLabels[0] = player1Label;
        playerLabels[1] = player2Label;
        playerLabels[2] = player3Label;
        playerLabels[3] = player4Label;
        playerLabels[4] = player5Label;
        playerLabels[5] = player6Label;
        
        //Match player property lists
        playerProperties[0] = jList1;
        playerProperties[1] = jList2;
        playerProperties[2] = jList3;
        playerProperties[3] = jList4;
        playerProperties[4] = jList5;
        playerProperties[5] = jList6;
        
        
        /** Match cell fields with GUI labels */
        CellLabels[0][0] = jLabel1;
        CellLabels[0][1] = jLabel2;
        CellLabels[0][2] = jLabel3;
        CellLabels[1][0] = jLabel4;
        CellLabels[1][1] = jLabel5;
        CellLabels[1][2] = jLabel6;
        CellLabels[2][0] = jLabel7;
        CellLabels[2][1] = jLabel8;
        CellLabels[2][2] = jLabel9;
        CellLabels[3][0] = jLabel10;
        CellLabels[3][1] = jLabel11;
        CellLabels[3][2] = jLabel12;
        CellLabels[4][0] = jLabel13;
        CellLabels[4][1] = jLabel14;
        CellLabels[4][2] = jLabel15;
        CellLabels[5][0] = jLabel16;
        CellLabels[5][1] = jLabel17;
        CellLabels[5][2] = jLabel18;
        CellLabels[6][0] = jLabel19;
        CellLabels[6][1] = jLabel20;
        CellLabels[6][2] = jLabel21;
        CellLabels[7][0] = jLabel22;
        CellLabels[7][1] = jLabel23;
        CellLabels[7][2] = jLabel24;
        CellLabels[8][0] = jLabel25;
        CellLabels[8][1] = jLabel26;
        CellLabels[8][2] = jLabel27;
        CellLabels[9][0] = jLabel28;
        CellLabels[9][1] = jLabel29;
        CellLabels[9][2] = jLabel30;
        CellLabels[10][0] = jLabel31;
        CellLabels[10][1] = jLabel32;
        CellLabels[10][2] = jLabel33;
        CellLabels[11][0] = jLabel34;
        CellLabels[11][1] = jLabel35;
        CellLabels[11][2] = jLabel36;
        CellLabels[12][0] = jLabel37;
        CellLabels[12][1] = jLabel38;
        CellLabels[12][2] = jLabel39;
        CellLabels[13][0] = jLabel40;
        CellLabels[13][1] = jLabel41;
        CellLabels[13][2] = jLabel42;
        CellLabels[14][0] = jLabel43;
        CellLabels[14][1] = jLabel44;
        CellLabels[14][2] = jLabel45;
        CellLabels[15][0] = jLabel46;
        CellLabels[15][1] = jLabel47;
        CellLabels[15][2] = jLabel48;
        CellLabels[16][0] = jLabel49;
        CellLabels[16][1] = jLabel50;
        CellLabels[16][2] = jLabel51;
        CellLabels[17][0] = jLabel52;
        CellLabels[17][1] = jLabel53;
        CellLabels[17][2] = jLabel54;
        CellLabels[18][0] = jLabel55;
        CellLabels[18][1] = jLabel56;
        CellLabels[18][2] = jLabel57;
        CellLabels[19][0] = jLabel58;
        CellLabels[19][1] = jLabel59;
        CellLabels[19][2] = jLabel60;
        CellLabels[20][0] = jLabel61;
        CellLabels[20][1] = jLabel62;
        CellLabels[20][2] = jLabel63;
        CellLabels[21][0] = jLabel64;
        CellLabels[21][1] = jLabel65;
        CellLabels[21][2] = jLabel66;
        CellLabels[22][0] = jLabel67;
        CellLabels[22][1] = jLabel68;
        CellLabels[22][2] = jLabel69;
        CellLabels[23][0] = jLabel70;
        CellLabels[23][1] = jLabel71;
        CellLabels[23][2] = jLabel72;
        CellLabels[24][0] = jLabel73;
        CellLabels[24][1] = jLabel74;
        CellLabels[24][2] = jLabel75;
        CellLabels[25][0] = jLabel76;
        CellLabels[25][1] = jLabel77;
        CellLabels[25][2] = jLabel78;
        CellLabels[26][0] = jLabel79;
        CellLabels[26][1] = jLabel80;
        CellLabels[26][2] = jLabel81;
        CellLabels[27][0] = jLabel82;
        CellLabels[27][1] = jLabel83;
        CellLabels[27][2] = jLabel84;
        CellLabels[28][0] = jLabel85;
        CellLabels[28][1] = jLabel86;
        CellLabels[28][2] = jLabel87;
        CellLabels[29][0] = jLabel88;
        CellLabels[29][1] = jLabel89;
        CellLabels[29][2] = jLabel90;
        CellLabels[30][0] = jLabel91;
        CellLabels[30][1] = jLabel92;
        CellLabels[30][2] = jLabel93;
        CellLabels[31][0] = jLabel94;
        CellLabels[31][1] = jLabel95;
        CellLabels[31][2] = jLabel96;
        CellLabels[32][0] = jLabel97;
        CellLabels[32][1] = jLabel98;
        CellLabels[32][2] = jLabel99;
        CellLabels[33][0] = jLabel100;
        CellLabels[33][1] = jLabel101;
        CellLabels[33][2] = jLabel102;
        CellLabels[34][0] = jLabel103;
        CellLabels[34][1] = jLabel104;
        CellLabels[34][2] = jLabel105;
        CellLabels[35][0] = jLabel106;
        CellLabels[35][1] = jLabel107;
        CellLabels[35][2] = jLabel108;
        CellLabels[36][0] = jLabel109;
        CellLabels[36][1] = jLabel110;
        CellLabels[36][2] = jLabel111;
        CellLabels[37][0] = jLabel112;
        CellLabels[37][1] = jLabel113;
        CellLabels[37][2] = jLabel114;
        CellLabels[38][0] = jLabel115;
        CellLabels[38][1] = jLabel116;
        CellLabels[38][2] = jLabel117;
        CellLabels[39][0] = jLabel118;
        CellLabels[39][1] = jLabel119;
        CellLabels[39][2] = jLabel120;
        
        //Color labels
        CellLabels[1][0].setForeground(Color.PINK);
        CellLabels[3][0].setForeground(Color.PINK);
        CellLabels[6][0].setForeground(Color.CYAN);
        CellLabels[8][0].setForeground(Color.CYAN);
        CellLabels[9][0].setForeground(Color.CYAN);
        CellLabels[11][0].setForeground(Color.MAGENTA);
        CellLabels[13][0].setForeground(Color.MAGENTA);
        CellLabels[14][0].setForeground(Color.MAGENTA);
        CellLabels[16][0].setForeground(Color.ORANGE);
        CellLabels[18][0].setForeground(Color.ORANGE);
        CellLabels[19][0].setForeground(Color.ORANGE);
        CellLabels[21][0].setForeground(Color.RED);
        CellLabels[23][0].setForeground(Color.RED);
        CellLabels[24][0].setForeground(Color.RED);
        CellLabels[26][0].setForeground(Color.YELLOW);
        CellLabels[27][0].setForeground(Color.YELLOW);
        CellLabels[29][0].setForeground(Color.YELLOW);
        CellLabels[31][0].setForeground(Color.GREEN);
        CellLabels[32][0].setForeground(Color.GREEN);
        CellLabels[34][0].setForeground(Color.GREEN);
        CellLabels[37][0].setForeground(Color.BLUE);
        CellLabels[39][0].setForeground(Color.BLUE);
        
        //List models
        this.listModels[0] = model1;
        this.listModels[1] = model2;
        this.listModels[2] = model3;
        this.listModels[3] = model4;
        this.listModels[4] = model5;
        this.listModels[5] = model6;
        this.updateGUI();
    }
    
    public void initBuyDialog() {
        int ctr;
        int ctr2;
        model7 = new DefaultListModel();
        model8 = new DefaultListModel();
        
        //Player's list
        for (ctr = 1; ctr <= numOfPlayers;ctr++) {
            if (ctr != gameMaster.getCurrentPlayer().getSymbol()) {
                model7.addElement(Integer.toString(ctr));
                this.playersList.setModel(model7);
            }
        }
        
        playersList.setSelectionInterval(0,0);
        propertiesList.setSelectionInterval(0,0);
        buyDialog.pack();
    }
    
    
    public void enableButtons(boolean[] en) {
        //SetEnabled to buttons
        buyButton.setEnabled(en[0]);
        rollDiceButton.setEnabled(rollDiceButtonEnabled);
        buildHouseButton.setEnabled(en[2]);
        useJailCardButton.setEnabled(en[1]);
        buyFromPlayerButton.setEnabled(en[4]);
        drawCardButton.setEnabled(en[3]);
    }
    
    public void updateGUI() {
        int ctr;
        cell[] gameCells = this.gameMaster.GUIreturnCells();
        buyDialog.setVisible(false);
        //Refresh cell labels to match cell name,price and owner
        for (ctr = 0; ctr < 40; ctr++) {
            CellLabels[ctr][0].setText(gameCells[ctr].getName());
            CellLabels[ctr][2].setText(" ");
            if (gameCells[ctr] instanceof landPropertyCell) {
                CellLabels[ctr][1].setText(Integer.toString(((landPropertyCell)gameCells[ctr]).getRent()));
                CellLabels[ctr][2].setText("Owner: "+Integer.toString(gameCells[ctr].getOwner()));
            }
            if (gameCells[ctr] instanceof railRoadCell) {
                CellLabels[ctr][1].setText(Integer.toString(((railRoadCell)gameCells[ctr]).getRent()));
                CellLabels[ctr][2].setText("Owner: "+Integer.toString(gameCells[ctr].getOwner()));
            }
            
            if (gameCells[ctr] instanceof utilityCell) {
                CellLabels[ctr][1].setText("zaria");
            }
        }
        
        currentPlayerLabel.setText("Paizei o Player "+Integer.toString(gameMaster.GUIreturnCurrentPlayer()));
        
        //Refresh cell info label
        if (gameMaster.GUIreturnPlayerLocation(gameMaster.GUIreturnCurrentPlayer()) instanceof landPropertyCell) {
            landPropertyCell t = ((landPropertyCell)gameMaster.GUIreturnPlayerLocation(gameMaster.GUIreturnCurrentPlayer()));
            info.setText("Price: "+Integer.toString(t.getPrice()) + " Building price: "+Integer.toString(t.getBuildingPrice()) + " Buildings: "+Integer.toString(t.getBuildings()));
        } else if (gameMaster.GUIreturnPlayerLocation(gameMaster.GUIreturnCurrentPlayer()) instanceof railRoadCell) {
            railRoadCell t = ((railRoadCell)gameMaster.GUIreturnPlayerLocation(gameMaster.GUIreturnCurrentPlayer()));
            info.setText("Price: "+Integer.toString(t.getPrice()));
        } else if (gameMaster.GUIreturnPlayerLocation(gameMaster.GUIreturnCurrentPlayer()) instanceof utilityCell) {
            utilityCell t = ((utilityCell)gameMaster.GUIreturnPlayerLocation(gameMaster.GUIreturnCurrentPlayer()));
            info.setText("Price: "+Integer.toString(t.getPrice()));
        } else {
            info.setText(" ");
        }
        
        //Refresh player labels with credit and location
        for (ctr=0;ctr<numOfPlayers;ctr++) {
            playerLabels[ctr].setText("Player "+(ctr+1)+": &"+Integer.toString(gameMaster.GUIreturnPlayerCredit(ctr+1))+" " +gameMaster.GUIreturnPlayerLocation(ctr+1).getName());
        }
        
        //Enable player labels and lists
        for (ctr=1;ctr<=numOfPlayers;ctr++) {
            if (gameMaster.getCurrentPlayer().getSymbol() == ctr) {
                listsEnabled[ctr-1] = true;
            } else {
                listsEnabled[ctr-1] = false;
            }
            System.out.println("Enabling "+(ctr-1)+"jList");
            playerLabels[ctr-1].setEnabled(listsEnabled[ctr-1]);
            playerProperties[ctr-1].setEnabled(listsEnabled[ctr-1]);
        }
        
        //Refresh player lists,make list models
        for (ctr = 1; ctr<=this.numOfPlayers; ctr++) {
            listModels[ctr-1].removeAllElements();
            int ctr2;
            String[] properties = gameMaster.GUIreturnPropertiesName(ctr);
            for (ctr2 =0;ctr2<properties.length;ctr2++) {
                this.listModels[ctr-1].addElement(properties[ctr2]);
                if (gameMaster.GUIreturnPlayer(ctr).hasJailCard() == true) this.listModels[ctr-1].addElement("Jail Card");
            }
            playerProperties[ctr-1].setModel(listModels[ctr-1]);
        }
        
        try {
            //Refresh control buttons enabled value
            rollDiceButton.setEnabled(rollDiceButtonEnabled);
            if (gameMaster.getCurrentPlayer().getLocation() instanceof utilityCell)  {
                rollDiceButtonEnabled = true;
            }
            enableButtons(gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).getLocation().execute(gameMaster));
        } catch (MonopolyException ex) {
            System.out.println("Kati pige strava...");
        }
        
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        buyDialog = new javax.swing.JDialog();
        jScrollPane7 = new javax.swing.JScrollPane();
        playersList = new javax.swing.JList();
        jLabel121 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        propertiesList = new javax.swing.JList();
        jLabel122 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        cardDialog = new javax.swing.JDialog();
        cardMessage = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        mortDialog = new javax.swing.JDialog();
        jScrollPane9 = new javax.swing.JScrollPane();
        jList7 = new javax.swing.JList();
        jButton3 = new javax.swing.JButton();
        jLabel123 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jLabel64 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        jLabel67 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        jPanel24 = new javax.swing.JPanel();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jPanel25 = new javax.swing.JPanel();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jLabel76 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jLabel82 = new javax.swing.JLabel();
        jLabel83 = new javax.swing.JLabel();
        jLabel84 = new javax.swing.JLabel();
        jPanel29 = new javax.swing.JPanel();
        jLabel85 = new javax.swing.JLabel();
        jLabel86 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        jPanel30 = new javax.swing.JPanel();
        jLabel88 = new javax.swing.JLabel();
        jLabel89 = new javax.swing.JLabel();
        jLabel90 = new javax.swing.JLabel();
        jPanel31 = new javax.swing.JPanel();
        jLabel91 = new javax.swing.JLabel();
        jLabel92 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        jPanel32 = new javax.swing.JPanel();
        jLabel94 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        jLabel96 = new javax.swing.JLabel();
        jPanel33 = new javax.swing.JPanel();
        jLabel97 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        jLabel99 = new javax.swing.JLabel();
        jPanel34 = new javax.swing.JPanel();
        jLabel100 = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        jLabel102 = new javax.swing.JLabel();
        jPanel35 = new javax.swing.JPanel();
        jLabel103 = new javax.swing.JLabel();
        jLabel104 = new javax.swing.JLabel();
        jLabel105 = new javax.swing.JLabel();
        jPanel36 = new javax.swing.JPanel();
        jLabel106 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        jLabel108 = new javax.swing.JLabel();
        jPanel37 = new javax.swing.JPanel();
        jLabel109 = new javax.swing.JLabel();
        jLabel110 = new javax.swing.JLabel();
        jLabel111 = new javax.swing.JLabel();
        jPanel38 = new javax.swing.JPanel();
        jLabel112 = new javax.swing.JLabel();
        jLabel113 = new javax.swing.JLabel();
        jLabel114 = new javax.swing.JLabel();
        jPanel39 = new javax.swing.JPanel();
        jLabel115 = new javax.swing.JLabel();
        jLabel116 = new javax.swing.JLabel();
        jLabel117 = new javax.swing.JLabel();
        jPanel40 = new javax.swing.JPanel();
        jLabel118 = new javax.swing.JLabel();
        jLabel119 = new javax.swing.JLabel();
        jLabel120 = new javax.swing.JLabel();
        player1Label = new javax.swing.JLabel();
        player2Label = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        player4Label = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jSeparator1 = new javax.swing.JSeparator();
        rollDiceButton = new javax.swing.JButton();
        useJailCardButton = new javax.swing.JButton();
        buildHouseButton = new javax.swing.JButton();
        drawCardButton = new javax.swing.JButton();
        endTurnButton = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList4 = new javax.swing.JList();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList3 = new javax.swing.JList();
        player3Label = new javax.swing.JLabel();
        jPanel27 = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        currentPlayerLabel = new javax.swing.JLabel();
        info = new javax.swing.JLabel();
        buyButton = new javax.swing.JButton();
        player5Label = new javax.swing.JLabel();
        player6Label = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jList5 = new javax.swing.JList();
        jScrollPane6 = new javax.swing.JScrollPane();
        jList6 = new javax.swing.JList();
        buyFromPlayerButton = new javax.swing.JButton();
        jLabel124 = new javax.swing.JLabel();

        buyDialog.getContentPane().setLayout(new java.awt.GridBagLayout());

        buyDialog.setTitle("Agora apo");
        playersList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                playersListValueChanged(evt);
            }
        });

        jScrollPane7.setViewportView(playersList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 107;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        buyDialog.getContentPane().add(jScrollPane7, gridBagConstraints);

        jLabel121.setText("Player:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        buyDialog.getContentPane().add(jLabel121, gridBagConstraints);

        jScrollPane8.setViewportView(propertiesList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.ipadx = 242;
        gridBagConstraints.ipady = 108;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        buyDialog.getContentPane().add(jScrollPane8, gridBagConstraints);

        jLabel122.setText("Property:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 204;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        buyDialog.getContentPane().add(jLabel122, gridBagConstraints);

        jButton1.setText("Buy");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 327;
        gridBagConstraints.ipady = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(18, 10, 10, 7);
        buyDialog.getContentPane().add(jButton1, gridBagConstraints);

        cardDialog.getContentPane().setLayout(new java.awt.GridBagLayout());

        cardMessage.setText("jLabel123");
        cardDialog.getContentPane().add(cardMessage, new java.awt.GridBagConstraints());

        jButton2.setText("OK");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        cardDialog.getContentPane().add(jButton2, gridBagConstraints);

        mortDialog.getContentPane().setLayout(new java.awt.GridBagLayout());

        mortDialog.setTitle("YPOTHIKEYISI  PERIOUSIAS");
        jList7.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList7ValueChanged(evt);
            }
        });

        jScrollPane9.setViewportView(jList7);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        mortDialog.getContentPane().add(jScrollPane9, gridBagConstraints);

        jButton3.setText("Ypothikeysi periousias");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        mortDialog.getContentPane().add(jButton3, gridBagConstraints);

        jLabel123.setText("Den exete arketa xrimata.Eiste ypoxreomenos na ypothikeysete kati");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        mortDialog.getContentPane().add(jLabel123, gridBagConstraints);

        getContentPane().setLayout(null);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Monopoly");
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                formComponentResized(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel1.setText("jLabel1");

        jLabel2.setText(" ");

        jLabel3.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel3)
                    .add(jLabel2)
                    .add(jLabel1))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel3)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel1);
        jPanel1.setBounds(800, 600, 80, 62);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel4.setForeground(new java.awt.Color(255, 51, 0));
        jLabel4.setText("jLabel1");

        jLabel6.setText("jLabel3");

        jLabel5.setText(" ");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel6)
                    .add(jLabel4)
                    .add(jLabel5))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel5)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel6)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel2);
        jPanel2.setBounds(720, 600, 80, 62);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel7.setText("jLabel1");

        jLabel8.setText(" ");

        jLabel9.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel9)
                    .add(jLabel8)
                    .add(jLabel7))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jLabel7)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel8)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel9)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel3);
        jPanel3.setBounds(640, 600, 80, 62);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel10.setText("jLabel1");

        jLabel11.setText(" ");

        jLabel12.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel12)
                    .add(jLabel10)
                    .add(jLabel11))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(jLabel10)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel11)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel12)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel4);
        jPanel4.setBounds(560, 600, 80, 62);

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel13.setText("jLabel1");

        jLabel14.setText(" ");

        jLabel15.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel15)
                    .add(jLabel14)
                    .add(jLabel13))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(jLabel13)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel14)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel15)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel5);
        jPanel5.setBounds(480, 600, 80, 62);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel16.setText("jLabel1");

        jLabel17.setText(" ");

        jLabel18.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel18)
                    .add(jLabel17)
                    .add(jLabel16))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(jLabel16)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel17)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel18)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel6);
        jPanel6.setBounds(400, 600, 80, 62);

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel19.setText("jLabel1");

        jLabel20.setText(" ");

        jLabel21.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel21)
                    .add(jLabel20)
                    .add(jLabel19))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(jLabel19)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel20)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel21)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel7);
        jPanel7.setBounds(320, 600, 80, 62);

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel22.setText("jLabel1");

        jLabel23.setText(" ");

        jLabel24.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel8Layout.createSequentialGroup()
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel24)
                    .add(jLabel23)
                    .add(jLabel22))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel8Layout.createSequentialGroup()
                .add(jLabel22)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel23)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel24)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel8);
        jPanel8.setBounds(240, 600, 80, 62);

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel25.setText("jLabel1");

        jLabel26.setText(" ");

        jLabel27.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel9Layout = new org.jdesktop.layout.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel9Layout.createSequentialGroup()
                .add(jPanel9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel27)
                    .add(jLabel26)
                    .add(jLabel25))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel9Layout.createSequentialGroup()
                .add(jLabel25)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel26)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel27)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel9);
        jPanel9.setBounds(160, 600, 80, 62);

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel28.setText("jLabel1");

        jLabel29.setText(" ");

        jLabel30.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel10Layout = new org.jdesktop.layout.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel10Layout.createSequentialGroup()
                .add(jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel30)
                    .add(jLabel29)
                    .add(jLabel28))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel10Layout.createSequentialGroup()
                .add(jLabel28)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel29)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel30)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel10);
        jPanel10.setBounds(80, 600, 80, 62);

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel31.setText("jLabel1");

        jLabel32.setText(" ");

        jLabel33.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel11Layout = new org.jdesktop.layout.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel11Layout.createSequentialGroup()
                .add(jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel33)
                    .add(jLabel32)
                    .add(jLabel31))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel11Layout.createSequentialGroup()
                .add(jLabel31)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel32)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel33)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel11);
        jPanel11.setBounds(0, 600, 80, 62);

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel34.setText("jLabel1");

        jLabel35.setText("    ");

        jLabel36.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel12Layout = new org.jdesktop.layout.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel12Layout.createSequentialGroup()
                .add(jPanel12Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel36)
                    .add(jLabel35)
                    .add(jLabel34))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel12Layout.createSequentialGroup()
                .add(jLabel34)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel35)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel36)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel12);
        jPanel12.setBounds(0, 540, 80, 62);

        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel37.setText("jLabel1");

        jLabel38.setText("    ");

        jLabel39.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel13Layout = new org.jdesktop.layout.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel13Layout.createSequentialGroup()
                .add(jPanel13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel39)
                    .add(jLabel37)
                    .add(jLabel38))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel13Layout.createSequentialGroup()
                .add(jLabel37)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel38)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel39)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel13);
        jPanel13.setBounds(0, 480, 80, 62);

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel40.setText("jLabel1");

        jLabel41.setText("    ");

        jLabel42.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel14Layout = new org.jdesktop.layout.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel14Layout.createSequentialGroup()
                .add(jPanel14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel42)
                    .add(jLabel41)
                    .add(jLabel40))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel14Layout.createSequentialGroup()
                .add(jLabel40)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel41)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel42)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel14);
        jPanel14.setBounds(0, 420, 80, 62);

        jPanel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel43.setText("jLabel1");

        jLabel44.setText("    ");

        jLabel45.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel15Layout = new org.jdesktop.layout.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel15Layout.createSequentialGroup()
                .add(jPanel15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel45)
                    .add(jLabel44)
                    .add(jLabel43))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel15Layout.createSequentialGroup()
                .add(jLabel43)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel44)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel45)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel15);
        jPanel15.setBounds(0, 360, 80, 62);

        jPanel16.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel46.setText("jLabel1");

        jLabel47.setText("    ");

        jLabel48.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel16Layout = new org.jdesktop.layout.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel16Layout.createSequentialGroup()
                .add(jPanel16Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel48)
                    .add(jLabel47)
                    .add(jLabel46))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel16Layout.createSequentialGroup()
                .add(jLabel46)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel47)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel48)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel16);
        jPanel16.setBounds(0, 300, 80, 62);

        jPanel17.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel49.setText("jLabel1");

        jLabel50.setText("    ");

        jLabel51.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel17Layout = new org.jdesktop.layout.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel17Layout.createSequentialGroup()
                .add(jPanel17Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel51)
                    .add(jLabel50)
                    .add(jLabel49))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel17Layout.createSequentialGroup()
                .add(jLabel49)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel50)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel51)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel17);
        jPanel17.setBounds(0, 240, 80, 62);

        jPanel18.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel52.setText("jLabel1");

        jLabel53.setText("    ");

        jLabel54.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel18Layout = new org.jdesktop.layout.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel18Layout.createSequentialGroup()
                .add(jPanel18Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel54)
                    .add(jLabel53)
                    .add(jLabel52))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel18Layout.createSequentialGroup()
                .add(jLabel52)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel53)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel54)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel18);
        jPanel18.setBounds(0, 180, 80, 62);

        jPanel19.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel55.setText("jLabel1");

        jLabel56.setText("    ");

        jLabel57.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel19Layout = new org.jdesktop.layout.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel19Layout.createSequentialGroup()
                .add(jPanel19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel57)
                    .add(jLabel55)
                    .add(jLabel56))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel19Layout.createSequentialGroup()
                .add(jLabel55)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel56)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel57)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel19);
        jPanel19.setBounds(0, 120, 80, 62);

        jPanel20.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel58.setText("jLabel1");

        jLabel59.setText("    ");

        jLabel60.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel20Layout = new org.jdesktop.layout.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel20Layout.createSequentialGroup()
                .add(jPanel20Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel60)
                    .add(jLabel58)
                    .add(jLabel59))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel20Layout.createSequentialGroup()
                .add(jLabel58)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel59)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel60)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel20);
        jPanel20.setBounds(0, 60, 80, 62);

        jPanel21.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel61.setText("jLabel1");
        jLabel61.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel61MouseMoved(evt);
            }
        });

        jLabel62.setText("    ");

        jLabel63.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel21Layout = new org.jdesktop.layout.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel21Layout.createSequentialGroup()
                .add(jPanel21Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel21Layout.createSequentialGroup()
                        .add(jPanel21Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel63)
                            .add(jLabel62, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE))
                        .add(20, 20, 20))
                    .add(jLabel61))
                .add(14, 14, 14))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel21Layout.createSequentialGroup()
                .add(jLabel61)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel62)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel63)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel21);
        jPanel21.setBounds(0, 0, 80, 62);

        jPanel22.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel64.setText("jLabel1");
        jLabel64.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel64MouseMoved(evt);
            }
        });

        jLabel65.setText("    ");

        jLabel66.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel22Layout = new org.jdesktop.layout.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel22Layout.createSequentialGroup()
                .add(jPanel22Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel66)
                    .add(jLabel65)
                    .add(jLabel64))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel22Layout.createSequentialGroup()
                .add(jLabel64)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel65)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel66)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel22);
        jPanel22.setBounds(80, 0, 80, 62);

        jPanel23.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel67.setText("jLabel1");

        jLabel68.setText("    ");

        jLabel69.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel23Layout = new org.jdesktop.layout.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel23Layout.createSequentialGroup()
                .add(jPanel23Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel69)
                    .add(jLabel68)
                    .add(jLabel67))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel23Layout.createSequentialGroup()
                .add(jLabel67)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel68)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel69)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel23);
        jPanel23.setBounds(160, 0, 80, 62);

        jPanel24.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel70.setText("jLabel1");

        jLabel71.setText("    ");

        jLabel72.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel24Layout = new org.jdesktop.layout.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel24Layout.createSequentialGroup()
                .add(jPanel24Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel72)
                    .add(jLabel71)
                    .add(jLabel70))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel24Layout.createSequentialGroup()
                .add(jLabel70)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel71)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel72)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel24);
        jPanel24.setBounds(240, 0, 80, 62);

        jPanel25.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel73.setText("jLabel1");

        jLabel74.setText("    ");

        jLabel75.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel25Layout = new org.jdesktop.layout.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel25Layout.createSequentialGroup()
                .add(jPanel25Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel75)
                    .add(jLabel74)
                    .add(jLabel73))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel25Layout.createSequentialGroup()
                .add(jLabel73)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel74)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel75)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel25);
        jPanel25.setBounds(320, 0, 80, 62);

        jPanel26.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel76.setText("jLabel1");

        jLabel77.setText("    ");

        jLabel78.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel26Layout = new org.jdesktop.layout.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel26Layout.createSequentialGroup()
                .add(jPanel26Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel78)
                    .add(jLabel76)
                    .add(jLabel77))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel26Layout.createSequentialGroup()
                .add(jLabel76)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel77)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel78)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel26);
        jPanel26.setBounds(400, 0, 80, 62);

        jPanel28.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel82.setText("jLabel1");

        jLabel83.setText("    ");

        jLabel84.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel28Layout = new org.jdesktop.layout.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel28Layout.createSequentialGroup()
                .add(jPanel28Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel84)
                    .add(jLabel83)
                    .add(jLabel82))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel28Layout.createSequentialGroup()
                .add(jLabel82)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel83)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel84)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel28);
        jPanel28.setBounds(560, 0, 80, 62);

        jPanel29.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel85.setText("jLabel1");

        jLabel86.setText("    ");

        jLabel87.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel29Layout = new org.jdesktop.layout.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel29Layout.createSequentialGroup()
                .add(jPanel29Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel87)
                    .add(jLabel86)
                    .add(jLabel85))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel29Layout.createSequentialGroup()
                .add(jLabel85)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel86)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel87)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel29);
        jPanel29.setBounds(640, 0, 80, 62);

        jPanel30.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel88.setText("jLabel1");

        jLabel89.setText("    ");

        jLabel90.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel30Layout = new org.jdesktop.layout.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel30Layout.createSequentialGroup()
                .add(jPanel30Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel90)
                    .add(jLabel89)
                    .add(jLabel88))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel30Layout.createSequentialGroup()
                .add(jLabel88)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel89)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel90)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel30);
        jPanel30.setBounds(720, 0, 80, 62);

        jPanel31.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel91.setText("jLabel1");

        jLabel92.setText("    ");

        jLabel93.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel31Layout = new org.jdesktop.layout.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel31Layout.createSequentialGroup()
                .add(jPanel31Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel93)
                    .add(jLabel92)
                    .add(jLabel91))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel31Layout.createSequentialGroup()
                .add(jLabel91)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel92)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel93)
                .addContainerGap())
        );
        getContentPane().add(jPanel31);
        jPanel31.setBounds(800, 0, 80, 62);

        jPanel32.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel94.setText("jLabel1");

        jLabel95.setText("    ");

        jLabel96.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel32Layout = new org.jdesktop.layout.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel32Layout.createSequentialGroup()
                .add(jPanel32Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel96)
                    .add(jLabel95)
                    .add(jLabel94))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel32Layout.createSequentialGroup()
                .add(jLabel94)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel95)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel96)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel32);
        jPanel32.setBounds(800, 60, 80, 62);

        jPanel33.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel97.setText("jLabel1");

        jLabel98.setText("    ");

        jLabel99.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel33Layout = new org.jdesktop.layout.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel33Layout.createSequentialGroup()
                .add(jPanel33Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel99)
                    .add(jLabel98)
                    .add(jLabel97))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel33Layout.createSequentialGroup()
                .add(jLabel97)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel98)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel99)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel33);
        jPanel33.setBounds(800, 120, 80, 62);

        jPanel34.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel100.setText("jLabel1");

        jLabel101.setText("    ");

        jLabel102.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel34Layout = new org.jdesktop.layout.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel34Layout.createSequentialGroup()
                .add(jPanel34Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel102)
                    .add(jLabel101)
                    .add(jLabel100))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel34Layout.createSequentialGroup()
                .add(jLabel100)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel101)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel102)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel34);
        jPanel34.setBounds(800, 180, 80, 62);

        jPanel35.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel103.setText("jLabel1");

        jLabel104.setText("    ");

        jLabel105.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel35Layout = new org.jdesktop.layout.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel35Layout.createSequentialGroup()
                .add(jPanel35Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel105)
                    .add(jLabel104)
                    .add(jLabel103))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel35Layout.createSequentialGroup()
                .add(jLabel103)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel104)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel105)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel35);
        jPanel35.setBounds(800, 240, 80, 62);

        jPanel36.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel106.setText("jLabel1");

        jLabel107.setText("    ");

        jLabel108.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel36Layout = new org.jdesktop.layout.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel36Layout.createSequentialGroup()
                .add(jPanel36Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel108)
                    .add(jLabel106)
                    .add(jLabel107))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel36Layout.createSequentialGroup()
                .add(jLabel106)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel107)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel108)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel36);
        jPanel36.setBounds(800, 300, 80, 62);

        jPanel37.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel109.setText("jLabel1");

        jLabel110.setText("    ");

        jLabel111.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel37Layout = new org.jdesktop.layout.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel37Layout.createSequentialGroup()
                .add(jPanel37Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel111)
                    .add(jLabel110)
                    .add(jLabel109))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel37Layout.createSequentialGroup()
                .add(jLabel109)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel110)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel111)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel37);
        jPanel37.setBounds(800, 360, 80, 62);

        jPanel38.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel112.setText("jLabel1");

        jLabel113.setText("    ");

        jLabel114.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel38Layout = new org.jdesktop.layout.GroupLayout(jPanel38);
        jPanel38.setLayout(jPanel38Layout);
        jPanel38Layout.setHorizontalGroup(
            jPanel38Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel38Layout.createSequentialGroup()
                .add(jPanel38Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel114)
                    .add(jLabel113)
                    .add(jLabel112))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel38Layout.setVerticalGroup(
            jPanel38Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel38Layout.createSequentialGroup()
                .add(jLabel112)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel113)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel114)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel38);
        jPanel38.setBounds(800, 420, 80, 62);

        jPanel39.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel115.setText("jLabel1");

        jLabel116.setText("    ");

        jLabel117.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel39Layout = new org.jdesktop.layout.GroupLayout(jPanel39);
        jPanel39.setLayout(jPanel39Layout);
        jPanel39Layout.setHorizontalGroup(
            jPanel39Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel39Layout.createSequentialGroup()
                .add(jPanel39Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel117)
                    .add(jLabel116)
                    .add(jLabel115))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel39Layout.setVerticalGroup(
            jPanel39Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel39Layout.createSequentialGroup()
                .add(jLabel115)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel116)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel117)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel39);
        jPanel39.setBounds(800, 480, 80, 62);

        jPanel40.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel118.setText("jLabel1");

        jLabel119.setText("    ");

        jLabel120.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel40Layout = new org.jdesktop.layout.GroupLayout(jPanel40);
        jPanel40.setLayout(jPanel40Layout);
        jPanel40Layout.setHorizontalGroup(
            jPanel40Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel40Layout.createSequentialGroup()
                .add(jPanel40Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel120)
                    .add(jLabel119)
                    .add(jLabel118))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel40Layout.setVerticalGroup(
            jPanel40Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel40Layout.createSequentialGroup()
                .add(jLabel118)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel119)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel120)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel40);
        jPanel40.setBounds(800, 540, 80, 62);

        player1Label.setText("Player 1");
        player1Label.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        player1Label.setEnabled(false);
        player1Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                player1LabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                player1LabelMouseExited(evt);
            }
        });

        getContentPane().add(player1Label);
        player1Label.setBounds(90, 70, 350, 20);

        player2Label.setText("Player 2");
        player2Label.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        player2Label.setEnabled(false);
        player2Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                player2LabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                player2LabelMouseExited(evt);
            }
        });

        getContentPane().add(player2Label);
        player2Label.setBounds(450, 70, 340, 20);

        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList1.setEnabled(false);
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jList1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jList1MouseClicked(evt);
            }
        });

        jScrollPane1.setViewportView(jList1);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(90, 90, 350, 80);

        player4Label.setText("Player 4");
        player4Label.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        player4Label.setEnabled(false);
        player4Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                player4LabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                player4LabelMouseExited(evt);
            }
        });

        getContentPane().add(player4Label);
        player4Label.setBounds(450, 180, 340, 20);

        jList2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList2.setEnabled(false);
        jList2.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList2ValueChanged(evt);
            }
        });

        jScrollPane2.setViewportView(jList2);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(450, 90, 340, 80);

        jSeparator1.setBackground(new java.awt.Color(102, 204, 255));
        getContentPane().add(jSeparator1);
        jSeparator1.setBounds(90, 420, 690, 10);

        rollDiceButton.setText("Rikse zaria");
        rollDiceButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rollDiceButtonMouseClicked(evt);
            }
        });

        getContentPane().add(rollDiceButton);
        rollDiceButton.setBounds(240, 430, 140, 30);

        useJailCardButton.setText("Xrisi kartas fylakis");
        useJailCardButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                useJailCardButtonMouseClicked(evt);
            }
        });

        getContentPane().add(useJailCardButton);
        useJailCardButton.setBounds(380, 430, 160, 30);

        buildHouseButton.setBackground(new java.awt.Color(204, 204, 255));
        buildHouseButton.setText("Ktisimo spitioy");
        buildHouseButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buildHouseButtonMouseClicked(evt);
            }
        });

        getContentPane().add(buildHouseButton);
        buildHouseButton.setBounds(90, 460, 290, 30);

        drawCardButton.setText("Sikose karta");
        drawCardButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                drawCardButtonMouseClicked(evt);
            }
        });

        getContentPane().add(drawCardButton);
        drawCardButton.setBounds(380, 460, 160, 30);

        endTurnButton.setText("Telos Gyrou");
        endTurnButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                endTurnButtonMouseClicked(evt);
            }
        });

        getContentPane().add(endTurnButton);
        endTurnButton.setBounds(90, 560, 700, 30);

        jList4.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList4.setEnabled(false);
        jList4.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList4ValueChanged(evt);
            }
        });

        jScrollPane4.setViewportView(jList4);

        getContentPane().add(jScrollPane4);
        jScrollPane4.setBounds(450, 200, 340, 80);

        jList3.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList3.setEnabled(false);
        jList3.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList3ValueChanged(evt);
            }
        });

        jScrollPane3.setViewportView(jList3);

        getContentPane().add(jScrollPane3);
        jScrollPane3.setBounds(90, 200, 350, 80);

        player3Label.setText("Player 3");
        player3Label.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        player3Label.setEnabled(false);
        player3Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                player3LabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                player3LabelMouseExited(evt);
            }
        });

        getContentPane().add(player3Label);
        player3Label.setBounds(90, 180, 350, 20);

        jPanel27.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel79.setText("jLabel1");

        jLabel80.setText("    ");

        jLabel81.setText("jLabel3");

        org.jdesktop.layout.GroupLayout jPanel27Layout = new org.jdesktop.layout.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel27Layout.createSequentialGroup()
                .add(jPanel27Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel81)
                    .add(jLabel80)
                    .add(jLabel79))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel27Layout.createSequentialGroup()
                .add(jLabel79)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel80)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel81)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        getContentPane().add(jPanel27);
        jPanel27.setBounds(480, 0, 80, 62);

        currentPlayerLabel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        currentPlayerLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(currentPlayerLabel);
        currentPlayerLabel.setBounds(90, 500, 310, 30);

        info.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));
        getContentPane().add(info);
        info.setBounds(400, 500, 390, 40);

        buyButton.setText("Agora Idioktisias");
        buyButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buyButtonMouseClicked(evt);
            }
        });

        getContentPane().add(buyButton);
        buyButton.setBounds(90, 430, 150, 30);

        player5Label.setText("Player 5");
        player5Label.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        player5Label.setEnabled(false);
        player5Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                player5LabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                player5LabelMouseExited(evt);
            }
        });

        getContentPane().add(player5Label);
        player5Label.setBounds(90, 290, 350, 20);

        player6Label.setText("Player 6");
        player6Label.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        player6Label.setEnabled(false);
        player6Label.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                player6LabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                player6LabelMouseExited(evt);
            }
        });

        getContentPane().add(player6Label);
        player6Label.setBounds(450, 290, 340, 20);

        jList5.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList5.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList5ValueChanged(evt);
            }
        });

        jScrollPane5.setViewportView(jList5);

        getContentPane().add(jScrollPane5);
        jScrollPane5.setBounds(90, 310, 350, 90);

        jList6.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList6.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList6ValueChanged(evt);
            }
        });

        jScrollPane6.setViewportView(jList6);

        getContentPane().add(jScrollPane6);
        jScrollPane6.setBounds(450, 310, 340, 90);

        buyFromPlayerButton.setText("Agora apo ton");
        buyFromPlayerButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buyFromPlayerButtonMouseClicked(evt);
            }
        });

        getContentPane().add(buyFromPlayerButton);
        buyFromPlayerButton.setBounds(540, 430, 250, 60);

        jLabel124.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jLabel124);
        jLabel124.setBounds(90, 530, 300, 20);

        pack();
    }//GEN-END:initComponents
    
    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        String sel = new String();
        sel = (String)jList7.getSelectedValue();
        propertyCell prop = gameMaster.getCurrentPlayer().findProperty(sel).getLand();
        gameMaster.getCurrentPlayer().getMoney(gameMaster.getCurrentPlayer().findProperty(sel).getLand().getMortPrice());
        gameMaster.getCurrentPlayer().findProperty(sel).getLand().makeMort();
        this.updateGUI();
    }//GEN-LAST:event_jButton3MouseClicked
    
    private void jList7ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList7ValueChanged
// TODO add your handling code here:
        
    }//GEN-LAST:event_jList7ValueChanged
    
    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        try {
            cardToExecute.execute(gameMaster);
            drawCardButtonEnabled = false;
            this.updateGUI();
            cardDialog.setVisible(true);
        } catch (noMoneyException ex) {
            initMortDialog();
            mortDialog.setVisible(true);
        }
    }//GEN-LAST:event_jButton2MouseClicked
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
    }//GEN-LAST:event_jButton2ActionPerformed
    
    private void jList6ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList6ValueChanged
        String selProperty = (String)jList6.getSelectedValue();
        
        if (selProperty == null) return;
        
        if (selProperty.contains("(YPOTHIKI)") == true) {
            StringBuffer p = new StringBuffer(selProperty);
            selProperty = p.substring(0,p.indexOf(("(YPOTHIKI")));
        }
        
        propertyCell prop;
        prop = null;
        
        if (listsEnabled[5] == false) return;
        if (selProperty != null) prop = (propertyCell)gameMaster.findCell(selProperty);
        
        if (selProperty !=null) {
            if (selProperty.contains("Jail") == true) {
                if (gameMaster.getCurrentPlayer().isJailed() == true) {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                    useJailCardButton.setEnabled(useJailCardButtonEnabled);
                } else {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                }
                buildHouseButtonEnabled = false;
            }
            
            else if (prop instanceof propertyCell) {
                sellToPlayerButtonEnabled = true;
                if (prop.checkIfMort() == true) {
                    unMortButtonEnabled = true;
                    setMortButtonEnabled = false;
                } else if (prop.checkIfMort() == false) {
                    setMortButtonEnabled = true;
                    unMortButtonEnabled = false;
                }
                
                if (prop instanceof landPropertyCell) {
                    info.setText("Houses: " + prop.getName() + " Rent: " + ((landPropertyCell)prop).getRent() + " Buildings: " + ((landPropertyCell)prop).getBuildings());
                    if (gameMaster.checkIfCanBuildHere((landPropertyCell)prop) == true || gameMaster.checkIfCanBuildHotelHere((landPropertyCell)prop) == true) {
                        buildHouseButtonEnabled = true;
                    } else buildHouseButtonEnabled = false;
                }
            }
        }
        buildHouseButton.setEnabled(buildHouseButtonEnabled);
        
    }//GEN-LAST:event_jList6ValueChanged
    
    private void jList5ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList5ValueChanged
        String selProperty = (String)jList5.getSelectedValue();
        
        if (selProperty == null) return;
        
        if (selProperty.contains("(YPOTHIKI)") == true) {
            StringBuffer p = new StringBuffer(selProperty);
            selProperty = p.substring(0,p.indexOf(("(YPOTHIKI")));
        }
        
        propertyCell prop;
        prop = null;
        
        if (listsEnabled[4] == false) return;
        if (selProperty != null) prop = (propertyCell)gameMaster.findCell(selProperty);
        
        if (selProperty !=null) {
            if (selProperty.contains("Jail") == true) {
                if (gameMaster.getCurrentPlayer().isJailed() == true) {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                    useJailCardButton.setEnabled(useJailCardButtonEnabled);
                } else {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                }
                buildHouseButtonEnabled = false;
            }
            
            else if (prop instanceof propertyCell) {
                sellToPlayerButtonEnabled = true;
                if (prop.checkIfMort() == true) {
                    unMortButtonEnabled = true;
                    setMortButtonEnabled = false;
                } else if (prop.checkIfMort() == false) {
                    setMortButtonEnabled = true;
                    unMortButtonEnabled = false;
                }
                
                if (prop instanceof landPropertyCell) {
                    info.setText("Houses: " + prop.getName() + " Rent: " + ((landPropertyCell)prop).getRent() + " Buildings: " + ((landPropertyCell)prop).getBuildings());
                    if (gameMaster.checkIfCanBuildHere((landPropertyCell)prop) == true || gameMaster.checkIfCanBuildHotelHere((landPropertyCell)prop) == true) {
                        buildHouseButtonEnabled = true;
                    } else buildHouseButtonEnabled = false;
                }
            }
        }
        buildHouseButton.setEnabled(buildHouseButtonEnabled);
        
    }//GEN-LAST:event_jList5ValueChanged
    
    private void jList4ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList4ValueChanged
        String selProperty = (String)jList3.getSelectedValue();
        
        if (selProperty == null) return;
        
        if (selProperty.contains("(YPOTHIKI)") == true) {
            StringBuffer p = new StringBuffer(selProperty);
            selProperty = p.substring(0,p.indexOf(("(YPOTHIKI")));
        }
        
        propertyCell prop;
        prop = null;
        
        if (listsEnabled[3] == false) return;
        if (selProperty != null) prop = (propertyCell)gameMaster.findCell(selProperty);
        
        if (selProperty !=null) {
            if (selProperty.contains("Jail") == true) {
                if (gameMaster.getCurrentPlayer().isJailed() == true) {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                    useJailCardButton.setEnabled(useJailCardButtonEnabled);
                } else {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                }
                buildHouseButtonEnabled = false;
            }
            
            else if (prop instanceof propertyCell) {
                sellToPlayerButtonEnabled = true;
                if (prop.checkIfMort() == true) {
                    unMortButtonEnabled = true;
                    setMortButtonEnabled = false;
                } else if (prop.checkIfMort() == false) {
                    setMortButtonEnabled = true;
                    unMortButtonEnabled = false;
                }
                
                if (prop instanceof landPropertyCell) {
                    info.setText("Houses: " + prop.getName() + " Rent: " + ((landPropertyCell)prop).getRent() + " Buildings: " + ((landPropertyCell)prop).getBuildings());
                    if (gameMaster.checkIfCanBuildHere((landPropertyCell)prop) == true || gameMaster.checkIfCanBuildHotelHere((landPropertyCell)prop) == true) {
                        buildHouseButtonEnabled = true;
                    } else buildHouseButtonEnabled = false;
                }
            }
        }
        buildHouseButton.setEnabled(buildHouseButtonEnabled);
        
    }//GEN-LAST:event_jList4ValueChanged
    
    private void jList2ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList2ValueChanged
// TODO add your handling code here:
        String selProperty = (String)jList2.getSelectedValue();
        
        if (selProperty == null) return;
        
        if (selProperty.contains("(YPOTHIKI)") == true) {
            StringBuffer p = new StringBuffer(selProperty);
            selProperty = p.substring(0,p.indexOf(("(YPOTHIKI")));
        }
        
        propertyCell prop;
        prop = null;
        
        if (listsEnabled[1] == false) return;
        if (selProperty != null) prop = (propertyCell)gameMaster.findCell(selProperty);
        
        if (selProperty !=null) {
            if (selProperty.contains("Jail") == true) {
                if (gameMaster.getCurrentPlayer().isJailed() == true) {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                    useJailCardButton.setEnabled(useJailCardButtonEnabled);
                } else {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                }
                buildHouseButtonEnabled = false;
            }
            
            else if (prop instanceof propertyCell) {
                sellToPlayerButtonEnabled = true;
                if (prop.checkIfMort() == true) {
                    unMortButtonEnabled = true;
                    setMortButtonEnabled = false;
                } else if (prop.checkIfMort() == false) {
                    setMortButtonEnabled = true;
                    unMortButtonEnabled = false;
                }
                
                if (prop instanceof landPropertyCell) {
                    info.setText("Houses: " + prop.getName() + " Rent: " + ((landPropertyCell)prop).getRent() + " Buildings: " + ((landPropertyCell)prop).getBuildings());
                    if (gameMaster.checkIfCanBuildHere((landPropertyCell)prop) == true || gameMaster.checkIfCanBuildHotelHere((landPropertyCell)prop) == true) {
                        buildHouseButtonEnabled = true;
                    } else buildHouseButtonEnabled = false;
                }
            }
        }
        buildHouseButton.setEnabled(buildHouseButtonEnabled);
        
    }//GEN-LAST:event_jList2ValueChanged
    
    public void initMortDialog() {
        int ctr;
        String[] properties = null;
        try {
            properties = gameMaster.getCurrentPlayer().GUIreturnPropertyTitlesNames();
        } catch (MonopolyException ex) {
            ex.printStackTrace();
        }
        if (properties == null) return;
        DefaultListModel mortModel = new DefaultListModel();
        for (ctr=0;ctr<properties.length;ctr++) {
            if (gameMaster.getCurrentPlayer().findProperty(properties[ctr]).getLand().checkIfMort()==false) {
                mortModel.addElement(properties[ctr]);
            }
        }
        jList7.setModel(mortModel);
        jList7.setSelectionInterval(0,0);
        if (mortModel.size()==0) {
            System.out.println("XREOKOPIA");
        }
        mortDialog.pack();
        mortDialog.setVisible(true);
    }
    
    public void initCardDialog() {
        cardMessage.setText(cardToExecute.getMessage());
        cardDialog.pack();
        this.updateGUI();
        
    }
    
    
    private void drawCardButtonMouseClicked(java.awt.event.MouseEvent evt)  {//GEN-FIRST:event_drawCardButtonMouseClicked
// TODO add your handling code here:
        this.cardToExecute = gameMaster.drawCard();
        this.initCardDialog();
        cardDialog.setVisible(true);
    }//GEN-LAST:event_drawCardButtonMouseClicked
    
    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        player seller = gameMaster.GUIreturnPlayer(Integer.parseInt((String)playersList.getSelectedValue()));
        int pIndex = Integer.parseInt((String)playersList.getSelectedValue());
        String property = (String)propertiesList.getSelectedValue();
        if (property == null) return;
        if (property.contains("kamia") == true) return;
        try {
            if (property.contains("Jail")==true) {
                gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).getJailCard(gameMaster.GUIreturnPlayer(pIndex).giveJailCard());
                this.updateGUI();
            } else {
                propertyCell prop = gameMaster.GUIreturnPlayer(pIndex).findProperty(property).getLand();
                gameMaster.sellProperty(seller,gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()),prop);
                this.updateGUI();
            }
        } catch (noMoneyException e) {
            initMortDialog();
            mortDialog.setVisible(true);
        }
        return;
        
    }//GEN-LAST:event_jButton1MouseClicked
    
    private void playersListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_playersListValueChanged
// TODO add your handling code here:
        if (playersList.getSelectedIndex() != -1)
            propertiesList.setModel(listModels[Integer.parseInt((String)playersList.getSelectedValue())-1]);
    }//GEN-LAST:event_playersListValueChanged
    
    private void jList1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jList1MouseClicked
// TODO add your handling code here:
        
    }//GEN-LAST:event_jList1MouseClicked
    
    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
// TODO add your handling code here:
        String selProperty = (String)jList1.getSelectedValue();
        
        if (selProperty == null) return;
        
        if (selProperty.contains("(YPOTHIKI)") == true) {
            StringBuffer p = new StringBuffer(selProperty);
            selProperty = p.substring(0,p.indexOf(("(YPOTHIKI")));
        }
        
        propertyCell prop;
        prop = null;
        
        if (listsEnabled[0] == false) return;
        if (selProperty != null) prop = (propertyCell)gameMaster.findCell(selProperty);
        
        if (selProperty !=null) {
            if (selProperty.contains("Jail") == true) {
                if (gameMaster.getCurrentPlayer().isJailed() == true) {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                    useJailCardButton.setEnabled(useJailCardButtonEnabled);
                } else {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                }
                buildHouseButtonEnabled = false;
            }
            
            else if (prop instanceof propertyCell) {
                sellToPlayerButtonEnabled = true;
                if (prop.checkIfMort() == true) {
                    unMortButtonEnabled = true;
                    setMortButtonEnabled = false;
                } else if (prop.checkIfMort() == false) {
                    setMortButtonEnabled = true;
                    unMortButtonEnabled = false;
                }
                
                if (prop instanceof landPropertyCell) {
                    info.setText("Houses: " + prop.getName() + " Rent: " + ((landPropertyCell)prop).getRent() + " Buildings: " + ((landPropertyCell)prop).getBuildings());
                    if (gameMaster.checkIfCanBuildHere((landPropertyCell)prop) == true || gameMaster.checkIfCanBuildHotelHere((landPropertyCell)prop) == true) {
                        buildHouseButtonEnabled = true;
                    } else buildHouseButtonEnabled = false;
                }
            }
        }
        buildHouseButton.setEnabled(buildHouseButtonEnabled);
        
    }//GEN-LAST:event_jList1ValueChanged
    
    private void buyFromPlayerButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buyFromPlayerButtonMouseClicked
// TODO add your handling code here:
        if (buyFromPlayerButtonEnabled == true) {
            this.initBuyDialog();
            buyDialog.setVisible(true);
        }
    }//GEN-LAST:event_buyFromPlayerButtonMouseClicked
    
    private void useJailCardButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_useJailCardButtonMouseClicked
// TODO add your handling code here:
        if (useJailCardButtonEnabled == true) {
            
        }
    }//GEN-LAST:event_useJailCardButtonMouseClicked
    
    private void player6LabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player6LabelMouseExited
// TODO add your handling code here:
    }//GEN-LAST:event_player6LabelMouseExited
    
    private void player6LabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player6LabelMouseEntered
// TODO add your handling code here:
    }//GEN-LAST:event_player6LabelMouseEntered
    
    private void player5LabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player5LabelMouseExited
// TODO add your handling code here:
    }//GEN-LAST:event_player5LabelMouseExited
    
    private void player5LabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player5LabelMouseEntered
// TODO add your handling code here:
    }//GEN-LAST:event_player5LabelMouseEntered
    
    private void buildHouseButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buildHouseButtonMouseClicked
        try{
            if (buildHouseButtonEnabled == true) {
                String t = (String)this.playerProperties[gameMaster.getCurrentPlayer().getSymbol()-1].getSelectedValue();
                landPropertyCell s = (landPropertyCell)gameMaster.getCurrentPlayer().findProperty(t).getLand();
                if (gameMaster.checkIfCanBuildHotelHere(s)==true)
                    try {gameMaster.buildHotel(s);
                    } catch (noHotelsException ex) {
                        System.out.println("No more hotels in bank");
                    } catch (noMoneyException ex) {
                        initMortDialog();
                        mortDialog.setVisible(true);
                        
                    } catch (noHousesException ex) {
                        System.out.println("No more hotels in bank");
                    } catch (hotelAlreadyException ex) {
                        ex.printStackTrace();
                    } else gameMaster.buildHouse(s);
                s = ((landPropertyCell)gameMaster.GUIreturnPlayerLocation(1));
                System.out.println(s.getBuildings());
                this.updateGUI();
            }
        } catch (fullHouseException e) {
            System.out.println("Cannot build more houses on this cell");
        } catch (noHousesException d) {
            System.out.println("No more houses in bank");
        }
    }//GEN-LAST:event_buildHouseButtonMouseClicked
    
    private void buyButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buyButtonMouseClicked
        try {
// TODO add your handling code here:
            if (buyButtonEnabled == true) {
                gameMaster.buyProperty();
                this.updateGUI();
            }
        } catch (noMoneyException ex) {
            initMortDialog();
            mortDialog.setVisible(true);
        }
        
    }//GEN-LAST:event_buyButtonMouseClicked
    
    private void endTurnButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_endTurnButtonMouseClicked
        gameMaster.nextPlayer();
        rollDiceButtonEnabled = true;
        this.updateGUI();
    }//GEN-LAST:event_endTurnButtonMouseClicked
    
    private void rollDiceButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rollDiceButtonMouseClicked
// TODO add your handling code here:
        int[] d = new int[2];
        if (rollDiceButtonEnabled == true) {
        d=gameMaster.rollDice();
            jLabel124.setText("Etyxes "+Integer.toString(d[0])+ " kai " + Integer.toString(d[1]));
            
            if (gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).isJailed() == false) {
                if (d[0]==d[1]) {
                    gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).threeDoubles++;
                    rollDiceButtonEnabled = true;
                    System.out.println("Threedoubles = " + gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).threeDoubles);
                    if (gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).threeDoubles == 3) {
                        gameMaster.goToJail();
                        gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).threeDoubles = 0;
                        this.updateGUI();
                        return;
                    }
                } else {
                    gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).threeDoubles = 0;
                    rollDiceButtonEnabled = false;
                }
            } else {
                if (d[0]==d[1]) gameMaster.GUIreturnPlayer(gameMaster.GUIreturnCurrentPlayer()).getOutOfJail();
                rollDiceButtonEnabled = false;
            }
            
            if (gameMaster.getCurrentPlayer().getLocation() instanceof utilityCell &&
                    gameMaster.getCurrentPlayer().getLocation().getOwner()>0 &&
                    gameMaster.getCurrentPlayer().getLocation().getOwner() != gameMaster.getCurrentPlayer().getSymbol()) {
                try {
                    gameMaster.getCurrentPlayer().giveMoneyToBank((d[0]+d[1])*4);
                    gameMaster.GUIreturnPlayer(gameMaster.getCurrentPlayer().getLocation().getOwner()).getMoney(((d[0]+d[1])*4));
                    gameMaster.move(gameMaster.findCell(d[0]+d[1]));
                    rollDiceButtonEnabled = false;
                } catch (noMoneyException ex) {
                    initMortDialog();
                    mortDialog.setVisible(true);
                }
            } else gameMaster.move(gameMaster.findCell(d[0]+d[1]));

            this.updateGUI();
            
        }
    }//GEN-LAST:event_rollDiceButtonMouseClicked
    
    private void player4LabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player4LabelMouseExited
        int nameLabelIndex;
        if (this.numOfPlayers < 4) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(4).getName()));
        CellLabels[nameLabelIndex][0].setForeground(old);
    }//GEN-LAST:event_player4LabelMouseExited
    
    private void player4LabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player4LabelMouseEntered
        int nameLabelIndex;
        if (this.numOfPlayers < 4) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(4).getName()));
        this.old = CellLabels[nameLabelIndex][0].getForeground();
        CellLabels[nameLabelIndex][0].setForeground(Color.CYAN);
    }//GEN-LAST:event_player4LabelMouseEntered
    
    private void player3LabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player3LabelMouseEntered
        int nameLabelIndex;
        if (this.numOfPlayers < 3) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(3).getName()));
        this.old = CellLabels[nameLabelIndex][0].getForeground();
        CellLabels[nameLabelIndex][0].setForeground(Color.CYAN);
    }//GEN-LAST:event_player3LabelMouseEntered
    
    private void player3LabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player3LabelMouseExited
        int nameLabelIndex;
        if (this.numOfPlayers < 3) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(3).getName()));
        CellLabels[nameLabelIndex][0].setForeground(old);
    }//GEN-LAST:event_player3LabelMouseExited
    
    private void player2LabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player2LabelMouseExited
        int nameLabelIndex;
        if (this.numOfPlayers < 2) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(2).getName()));
        CellLabels[nameLabelIndex][0].setForeground(old);
    }//GEN-LAST:event_player2LabelMouseExited
    
    private void player2LabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player2LabelMouseEntered
        int nameLabelIndex;
        if (this.numOfPlayers < 2) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(2).getName()));
        this.old = CellLabels[nameLabelIndex][0].getForeground();
        CellLabels[nameLabelIndex][0].setForeground(Color.CYAN);
    }//GEN-LAST:event_player2LabelMouseEntered
    
    private void player1LabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player1LabelMouseExited
        int nameLabelIndex;
        if (this.numOfPlayers < 1) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(1).getName()));
        CellLabels[nameLabelIndex][0].setForeground(old);
    }//GEN-LAST:event_player1LabelMouseExited
    
    private void player1LabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_player1LabelMouseEntered
        int nameLabelIndex;
        if (this.numOfPlayers < 1) return;
        nameLabelIndex = gameMaster.GUIreturnCellIndex(gameMaster.findCell(gameMaster.GUIreturnPlayerLocation(1).getName()));
        this.old = CellLabels[nameLabelIndex][0].getForeground();
        CellLabels[nameLabelIndex][0].setForeground(Color.CYAN);
    }//GEN-LAST:event_player1LabelMouseEntered
    
    private void jLabel64MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel64MouseMoved
        
    }//GEN-LAST:event_jLabel64MouseMoved
    
    private void jLabel61MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel61MouseMoved
        
    }//GEN-LAST:event_jLabel61MouseMoved
    
    private void jList3ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList3ValueChanged
        String selProperty = (String)jList3.getSelectedValue();
        
        if (selProperty == null) return;
        
        if (selProperty.contains("(YPOTHIKI)") == true) {
            StringBuffer p = new StringBuffer(selProperty);
            selProperty = p.substring(0,p.indexOf(("(YPOTHIKI")));
        }
        
        System.out.println(selProperty);
        propertyCell prop;
        prop = null;
        
        if (listsEnabled[2] == false) return;
        if (selProperty != null) prop = (propertyCell)gameMaster.findCell(selProperty);
        
        if (selProperty !=null) {
            if (selProperty.contains("Jail") == true) {
                if (gameMaster.getCurrentPlayer().isJailed() == true) {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                    useJailCardButton.setEnabled(useJailCardButtonEnabled);
                } else {
                    useJailCardButtonEnabled = true;
                    setMortButtonEnabled = false;
                    unMortButtonEnabled = false;
                    sellToPlayerButtonEnabled = true;
                }
                buildHouseButtonEnabled = false;
            }
            
            else if (prop instanceof propertyCell) {
                sellToPlayerButtonEnabled = true;
                if (prop.checkIfMort() == true) {
                    System.out.println("ypothiki");
                    unMortButtonEnabled = true;
                    setMortButtonEnabled = false;
                } else if (prop.checkIfMort() == false) {
                    System.out.println("oxi ypothiki");
                    setMortButtonEnabled = true;
                    unMortButtonEnabled = false;
                }
                
                if (prop instanceof landPropertyCell) {
                    info.setText("Houses: " + prop.getName() + " Rent: " + ((landPropertyCell)prop).getRent() + " Buildings: " + ((landPropertyCell)prop).getBuildings());
                    if (gameMaster.checkIfCanBuildHere((landPropertyCell)prop) == true || gameMaster.checkIfCanBuildHotelHere((landPropertyCell)prop) == true) {
                        buildHouseButtonEnabled = true;
                    } else buildHouseButtonEnabled = false;
                }
            }
        }
        buildHouseButton.setEnabled(buildHouseButtonEnabled);
        
    }//GEN-LAST:event_jList3ValueChanged
    
    private void formComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentResized
// TODO add your handling code here:
        
    }//GEN-LAST:event_formComponentResized
    
    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new board().setVisible(true);
//            }
//        });
//    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton buildHouseButton;
    public javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.ButtonGroup buttonGroup2;
    public javax.swing.JButton buyButton;
    public javax.swing.JDialog buyDialog;
    public javax.swing.JButton buyFromPlayerButton;
    public javax.swing.JDialog cardDialog;
    public javax.swing.JLabel cardMessage;
    public javax.swing.JLabel currentPlayerLabel;
    public javax.swing.JButton drawCardButton;
    public javax.swing.JButton endTurnButton;
    public javax.swing.JLabel info;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel10;
    public javax.swing.JLabel jLabel100;
    public javax.swing.JLabel jLabel101;
    public javax.swing.JLabel jLabel102;
    public javax.swing.JLabel jLabel103;
    public javax.swing.JLabel jLabel104;
    public javax.swing.JLabel jLabel105;
    public javax.swing.JLabel jLabel106;
    public javax.swing.JLabel jLabel107;
    public javax.swing.JLabel jLabel108;
    public javax.swing.JLabel jLabel109;
    public javax.swing.JLabel jLabel11;
    public javax.swing.JLabel jLabel110;
    public javax.swing.JLabel jLabel111;
    public javax.swing.JLabel jLabel112;
    public javax.swing.JLabel jLabel113;
    public javax.swing.JLabel jLabel114;
    public javax.swing.JLabel jLabel115;
    public javax.swing.JLabel jLabel116;
    public javax.swing.JLabel jLabel117;
    public javax.swing.JLabel jLabel118;
    public javax.swing.JLabel jLabel119;
    public javax.swing.JLabel jLabel12;
    public javax.swing.JLabel jLabel120;
    public javax.swing.JLabel jLabel121;
    public javax.swing.JLabel jLabel122;
    public javax.swing.JLabel jLabel123;
    public javax.swing.JLabel jLabel124;
    public javax.swing.JLabel jLabel13;
    public javax.swing.JLabel jLabel14;
    public javax.swing.JLabel jLabel15;
    public javax.swing.JLabel jLabel16;
    public javax.swing.JLabel jLabel17;
    public javax.swing.JLabel jLabel18;
    public javax.swing.JLabel jLabel19;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel20;
    public javax.swing.JLabel jLabel21;
    public javax.swing.JLabel jLabel22;
    public javax.swing.JLabel jLabel23;
    public javax.swing.JLabel jLabel24;
    public javax.swing.JLabel jLabel25;
    public javax.swing.JLabel jLabel26;
    public javax.swing.JLabel jLabel27;
    public javax.swing.JLabel jLabel28;
    public javax.swing.JLabel jLabel29;
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel30;
    public javax.swing.JLabel jLabel31;
    public javax.swing.JLabel jLabel32;
    public javax.swing.JLabel jLabel33;
    public javax.swing.JLabel jLabel34;
    public javax.swing.JLabel jLabel35;
    public javax.swing.JLabel jLabel36;
    public javax.swing.JLabel jLabel37;
    public javax.swing.JLabel jLabel38;
    public javax.swing.JLabel jLabel39;
    public javax.swing.JLabel jLabel4;
    public javax.swing.JLabel jLabel40;
    public javax.swing.JLabel jLabel41;
    public javax.swing.JLabel jLabel42;
    public javax.swing.JLabel jLabel43;
    public javax.swing.JLabel jLabel44;
    public javax.swing.JLabel jLabel45;
    public javax.swing.JLabel jLabel46;
    public javax.swing.JLabel jLabel47;
    public javax.swing.JLabel jLabel48;
    public javax.swing.JLabel jLabel49;
    public javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabel50;
    public javax.swing.JLabel jLabel51;
    public javax.swing.JLabel jLabel52;
    public javax.swing.JLabel jLabel53;
    public javax.swing.JLabel jLabel54;
    public javax.swing.JLabel jLabel55;
    public javax.swing.JLabel jLabel56;
    public javax.swing.JLabel jLabel57;
    public javax.swing.JLabel jLabel58;
    public javax.swing.JLabel jLabel59;
    public javax.swing.JLabel jLabel6;
    public javax.swing.JLabel jLabel60;
    public javax.swing.JLabel jLabel61;
    public javax.swing.JLabel jLabel62;
    public javax.swing.JLabel jLabel63;
    public javax.swing.JLabel jLabel64;
    public javax.swing.JLabel jLabel65;
    public javax.swing.JLabel jLabel66;
    public javax.swing.JLabel jLabel67;
    public javax.swing.JLabel jLabel68;
    public javax.swing.JLabel jLabel69;
    public javax.swing.JLabel jLabel7;
    public javax.swing.JLabel jLabel70;
    public javax.swing.JLabel jLabel71;
    public javax.swing.JLabel jLabel72;
    public javax.swing.JLabel jLabel73;
    public javax.swing.JLabel jLabel74;
    public javax.swing.JLabel jLabel75;
    public javax.swing.JLabel jLabel76;
    public javax.swing.JLabel jLabel77;
    public javax.swing.JLabel jLabel78;
    public javax.swing.JLabel jLabel79;
    public javax.swing.JLabel jLabel8;
    public javax.swing.JLabel jLabel80;
    public javax.swing.JLabel jLabel81;
    public javax.swing.JLabel jLabel82;
    public javax.swing.JLabel jLabel83;
    public javax.swing.JLabel jLabel84;
    public javax.swing.JLabel jLabel85;
    public javax.swing.JLabel jLabel86;
    public javax.swing.JLabel jLabel87;
    public javax.swing.JLabel jLabel88;
    public javax.swing.JLabel jLabel89;
    public javax.swing.JLabel jLabel9;
    public javax.swing.JLabel jLabel90;
    public javax.swing.JLabel jLabel91;
    public javax.swing.JLabel jLabel92;
    public javax.swing.JLabel jLabel93;
    public javax.swing.JLabel jLabel94;
    public javax.swing.JLabel jLabel95;
    public javax.swing.JLabel jLabel96;
    public javax.swing.JLabel jLabel97;
    public javax.swing.JLabel jLabel98;
    public javax.swing.JLabel jLabel99;
    public javax.swing.JList jList1;
    public javax.swing.JList jList2;
    public javax.swing.JList jList3;
    public javax.swing.JList jList4;
    public javax.swing.JList jList5;
    public javax.swing.JList jList6;
    public javax.swing.JList jList7;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel10;
    public javax.swing.JPanel jPanel11;
    public javax.swing.JPanel jPanel12;
    public javax.swing.JPanel jPanel13;
    public javax.swing.JPanel jPanel14;
    public javax.swing.JPanel jPanel15;
    public javax.swing.JPanel jPanel16;
    public javax.swing.JPanel jPanel17;
    public javax.swing.JPanel jPanel18;
    public javax.swing.JPanel jPanel19;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanel20;
    public javax.swing.JPanel jPanel21;
    public javax.swing.JPanel jPanel22;
    public javax.swing.JPanel jPanel23;
    public javax.swing.JPanel jPanel24;
    public javax.swing.JPanel jPanel25;
    public javax.swing.JPanel jPanel26;
    public javax.swing.JPanel jPanel27;
    public javax.swing.JPanel jPanel28;
    public javax.swing.JPanel jPanel29;
    public javax.swing.JPanel jPanel3;
    public javax.swing.JPanel jPanel30;
    public javax.swing.JPanel jPanel31;
    public javax.swing.JPanel jPanel32;
    public javax.swing.JPanel jPanel33;
    public javax.swing.JPanel jPanel34;
    public javax.swing.JPanel jPanel35;
    public javax.swing.JPanel jPanel36;
    public javax.swing.JPanel jPanel37;
    public javax.swing.JPanel jPanel38;
    public javax.swing.JPanel jPanel39;
    public javax.swing.JPanel jPanel4;
    public javax.swing.JPanel jPanel40;
    public javax.swing.JPanel jPanel5;
    public javax.swing.JPanel jPanel6;
    public javax.swing.JPanel jPanel7;
    public javax.swing.JPanel jPanel8;
    public javax.swing.JPanel jPanel9;
    public javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JScrollPane jScrollPane4;
    public javax.swing.JScrollPane jScrollPane5;
    public javax.swing.JScrollPane jScrollPane6;
    public javax.swing.JScrollPane jScrollPane7;
    public javax.swing.JScrollPane jScrollPane8;
    public javax.swing.JScrollPane jScrollPane9;
    public javax.swing.JSeparator jSeparator1;
    public javax.swing.JDialog mortDialog;
    public javax.swing.JLabel player1Label;
    public javax.swing.JLabel player2Label;
    public javax.swing.JLabel player3Label;
    public javax.swing.JLabel player4Label;
    public javax.swing.JLabel player5Label;
    public javax.swing.JLabel player6Label;
    public javax.swing.JList playersList;
    public javax.swing.JList propertiesList;
    public javax.swing.JButton rollDiceButton;
    public javax.swing.JButton useJailCardButton;
    // End of variables declaration//GEN-END:variables
    
}
