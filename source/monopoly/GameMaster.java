package monopoly;
import  gui.*;
import java.util.Vector;
public class GameMaster 
{
	private cell[] cells;
	private player[] players;	
	private Vector commandCards;
	private Vector decisionCards;	
	private boolean[] delay;
	private int currentPlayer;
	private int round;
	private bank trapeza;
	private dice gDice = new dice();
	
	
	/**
	 * Constructs the GameMaster(!) for custom play 
	 * Signature:GameMaster_int
	 * Type:Constructor
	 * 
	 * @param players
	 *            the players of the game
	 * 
	 */
	public GameMaster(int numberofplayers) {
		int cnt;

		// Initialize players
		this.players=new player[numberofplayers+1];
		for (cnt = 1; cnt <= numberofplayers; cnt++) {
			players[cnt] = new player(cnt);                        
		}
		
		// Initialize cells
		this.cells = new cell[40];
		
		this.cells[0] = new startCell();
		this.cells[1] = new landPropertyCell ("Leoforos Mesogeion","fouksia",60,2,50);
		this.cells[2] = new cardCell ("APOFASI",false);
		this.cells[3] = new landPropertyCell ("Odos Panepistimiou","fouksia",50,4,50);
		this.cells[4] = new taxCell ("Foros Eisodimatos",20);
		this.cells[5] = new railRoadCell ("Sidirodromikos stathmos eksoterikou",200,50);
		this.cells[6] = new landPropertyCell ("Ethniki Odos Athinon Korinthou","galazio",60,4,50);
		this.cells[7] = new cardCell ("ENTOLH",true);
		this.cells[8] = new landPropertyCell ("Leoforos Kifisias","galazio",100,6,50);
		this.cells[9] = new landPropertyCell ("Leoforos Athinon","galazio",120,6,50);
		this.cells[10] = new freeCell("FYLAKH");
		this.cells[11] = new landPropertyCell ("Ethniki odos Athinon Lamias","mov",120,8,50);
		this.cells[12] = new utilityCell ("Hlektiki etaireia",150);
		this.cells[13] = new landPropertyCell ("Odos Eyripidous","mov",140,10,100);
		this.cells[14] = new landPropertyCell ("Plateia Eleytherias", "mov",140,10,100);
		this.cells[15] = new railRoadCell ("Sidirodromikos stathmos Athinon Thessalonikis",200,50);
		this.cells[16] = new landPropertyCell ("Plateia Kaniggos","portokali", 160,12,100);
		this.cells[17] = new cardCell ("APOFASH",false);
		this.cells[18] = new landPropertyCell ("Leoforos Syggrou","portokali",180,14,100);
		this.cells[19] = new landPropertyCell ("Plateia Kolonakiou","portokali",180,14,100);
		this.cells[20] = new freeCell ("ELEYTHERI STATHMEYSI");
		this.cells[21] = new landPropertyCell ("Odos G Septemvriou", "kokkino",200,16,100);
		this.cells[22] = new cardCell ("ENTOLH",true);
		this.cells[23] = new landPropertyCell ("Odos Patision","kokkino",220,18,150);
		this.cells[24] = new landPropertyCell ("Odos Akadimias","kokkino",220,150,18);
		this.cells[25] = new railRoadCell ("Sidirodromikos Stathmos Peloponisou",200,50);
		this.cells[26] = new landPropertyCell ("Odos Akropoleos","kitrino",240,20,150);
		this.cells[27] = new landPropertyCell ("Odos Aiolou", "kitrino",260,22,150);
		this.cells[28] = new utilityCell ("Etaireia Ydaton",150);
		this.cells[29] = new landPropertyCell ("Odos Ermou","kitrino",260,22,150);
		this.cells[30] = new goToJailCell();
		this.cells[31] = new landPropertyCell ("Plateia Klaythmonos", "prasino",280,24,150);
		this.cells[32] = new landPropertyCell ("Egnatia Odos","prasino",300,26,200);
		this.cells[33] = new cardCell ("APOFASI", false);
		this.cells[34] = new landPropertyCell ("Odos Stadiou","prasino",300,26,200);
		this.cells[35] = new railRoadCell ("Sidirodromikos stathmos Athinon Xalkidos",200,50);
		this.cells[36] = new cardCell ("ENTOLH", true);
		this.cells[37] = new landPropertyCell ("Plateia Omonoias", "ble",320,28,200);
		this.cells[38] = new taxCell ("Foros polyteleias",100);
		this.cells[39] = new landPropertyCell ("Leoforos Tatoiou","ble",350,35,200);
                
                //Put all players at start
                for (cnt = 1;cnt<=numberofplayers;cnt++) {
                    players[cnt].changeLocation(this.cells[0]);
                }
                
                //Set initial player 1
                this.currentPlayer = 1;
		
                //Instantiate bank,propertyTitles and give them to bank
                this.trapeza = new bank();
                for (cnt=0;cnt<40;cnt++) {
                    if (this.cells[cnt] instanceof propertyCell) {
                        this.trapeza.takeTitle(new propertyTitle(cells[cnt].getName(),(propertyCell)cells[cnt]));
                    }
                }
		// Initialize cards
                this.commandCards = new Vector();
                this.decisionCards = new Vector();

                this.commandCards.addElement(new moveToCellCard("Pigaine sti leoforo tatoiou",this.cells[39]));
                this.commandCards.addElement(new moveToCellCard("Pigaine stin odo akadimias",this.cells[24]));
                this.commandCards.addElement(new moveToCellCard("Pigaine stin ethniki odo athinwn lamias,an pernas apo tin afeteria pare 200 euro",this.cells[11]));
                this.commandCards.addElement(new moveToCellCard("Pigaine stin ekkinisi",this.cells[0]));
                this.commandCards.addElement(new moveToCellCard("Pigaine ston sidirodromiko stathmo eksoterikou, an pernas apo tin ekkinisi pare 200 euro",this.cells[5]));
                this.commandCards.addElement(new moveToClosestRailRoadCard("Pigaine ston kontinotrero sidirodromiko stathmo kai plirwse ston idioktiti tou 2 fores to enoikio pou dikaioutai.An o sthathmos den exei poulithei mporei na ton agoraseis apo ti trapeza"));
                this.commandCards.addElement(new moveStepsCard("pigaine pisw 3 tetragwna",-3));
                this.commandCards.addElement(new moveToClosestUtilityCard("pigaine sti plisiesteri ipiresia,an den exei poulithei mporeis na tin agoraseis apo ti trapeza.An exei poulithei tote rikse to zari kai plirwse ston idioktiti tou 10 fores auto pou deixnoun ta zaria"));
                this.commandCards.addElement(new bankMoneyCard("i oikodomi kai to daneio sou wrimazoun.Pare 150 euro",150,true));
                this.commandCards.addElement(new bankMoneyCard("plirwse sinisfora ston erano iper twn ftwxwn 12 euro",12,false));
                this.commandCards.addElement(new bankMoneyCard("pare xristougeniatiko dwro 100 euro",100,true));
                this.commandCards.addElement(new bankMoneyCard("i trapeza sou plirwnei merisma 50 euro",50,true));
                this.commandCards.addElement(new playerMoneyCard("exeis eklegei proedros tou paixnidiou plirwse se kathe paixti 50 euro",50,false));
                this.commandCards.addElement(new payPlayerBuildingsCard("kane genikes episkeues se ola ta spitia sou.plirwse 25 euro gia kathe sou spiti kai 100 euro gia kathe sou ksenodoxeio"));
                this.commandCards.addElement(new jailCard("pigaine kateutheian sti filaki.Den pairneis 200 euro",true));
                this.commandCards.addElement(new jailCard("bges dwrean apo ti filaki",false));
                
                this.decisionCards.addElement(new moveToCellCard("pigaine stin ekkinisi",this.cells[0]));
                this.decisionCards.addElement(new bankMoneyCard("asfaleia zwis.pare 100 euro",100,true));
                this.decisionCards.addElement(new bankMoneyCard("apo tous tokous twn xrimatwn lamvaneis 45 euro",45,true));
                this.decisionCards.addElement(new bankMoneyCard("eksoda giatrou.plirwse 50 euro",50,false));
                this.decisionCards.addElement(new bankMoneyCard("plirwse gia nosokomiaki perithalpsi 100 euro",100,false));
                this.decisionCards.addElement(new bankMoneyCard("klironomeis 100 euro",100,true));
                this.decisionCards.addElement(new bankMoneyCard("apo ti polisi apothematos pairneis 45 euro",45,true));
                this.decisionCards.addElement(new bankMoneyCard("pare gia tis ipirisies pou proseferes 25 euro",25,true));
                this.decisionCards.addElement(new bankMoneyCard("kerdises to deutero vraveio omorfias .pairneis 11 euro",11,true));
                this.decisionCards.addElement(new bankMoneyCard("pistrofi forou eisodimatos.pairneis 20 euro",20,true));
                this.decisionCards.addElement(new bankMoneyCard("teleiwses o kanonas tou xrisou.pare 50 euro",50,true));
                this.decisionCards.addElement(new bankMoneyCard("trapeziko lathos iper sou.pare 200 euro",200,true));
                this.decisionCards.addElement(new payPlayerBuildingsCard("forologia gia tin anakatasteui ton odwn.plirwse 40 euro gia kathe spiti kai 115 gia kathe ksenodoxeio"));
                this.decisionCards.addElement(new playerMoneyCard("anoigma tis megalis operas pare 50 euro apo kathe paixti gia na kleiseis eisitiria",50,true));
                this.decisionCards.addElement(new jailCard("pigaine kateutheian sti filaki.Den pairneis 200 euro",true));
                this.decisionCards.addElement(new jailCard("bges dwrean apo ti filaki",false));
                
                
                
                
	}
		
	
	/**
	 * Constructs the GameMaster for quickplay Signature:GameMaster_int_int
	 * Type:Constructor
	 * 
	 * @param players
	 *            the players of the game
	 * @param rounds
	 *            the rounds to be played
	 * 
	 */
	public GameMaster(int players,int rounds){}
	
        public player getCurrentPlayer() {
            return this.players[this.currentPlayer];
        }
        
	/**
	 * Current player draws command card and executes it Signature
	 * void_drawCommandCard type: transformer
	 */
	private card drawCommandCard() {
		card cCard;
		int indexR = (int) (0+Math.ceil( (Math.random()*(commandCards.size()-1))));

		cCard = (card) commandCards.remove(0);

		if (cCard.getMessage().contains("fyge")) {
			this.players[currentPlayer].getJailCard((jailCard)cCard);
                        this.commandCards.remove(cCard);
			return cCard;
		}	
		commandCards.addElement(cCard);
		return cCard;

	}

     /**
     * Checks if player passes from start cell
     * Signature: boolean_compareIndex_cell_cell
     * @return true if passes, false if not
     * type: accessor     
     */
	public boolean compareIndex(cell a, cell dest) {
		int index1=0;
		int index2=0;

		while  (this.cells[index1] != a) {
                    if (index1==40) index1 = 0;
                    else index1++;
		}
		
		while (this.cells[index2]!= dest) {
                        if (index2 == 40) index2 = 0;
                        else index2++;
		}
		if (index2-index1<0)
			return true;		// perase afeteria
		else
			return false;		// den perase afeteria
	}
	
	/**
	 * Draw a card and return it,according to type of cell
	 * Signature: card_drawCard
	 * Type:modifier
	 * @return card Card drawn
	 */
        public card drawCard() {
            cardCell p;
            p = (cardCell)this.getCurrentPlayer().getLocation();
            if (p.checkCommand()==true) {
                return drawCommandCard();
            }
            else {
                return drawDecisionCard();
            }
        }
        
	/**
	 * Current player draws decision card and repushes it in stack
	 * void_drawCommandCard type: transformer
	 * @throws noMoneyException 
	 */
	private card drawDecisionCard() {
		card cCard;
		int indexR = (int) (0+Math.ceil( (Math.random()*(commandCards.size()-1))));
		cCard = (card) decisionCards.remove(indexR);
		
		if (cCard.getMessage().contains("fyge")) {
			this.players[currentPlayer].getJailCard((jailCard)cCard);
			commandCards.remove(cCard);
			return cCard;
		}		
		
		decisionCards.addElement(cCard);
                return cCard;
	}
	
	/**
	 * Goes the player at destination cell Signature:void_move_player_cell
	 * Type:transformer
	 * 
	 * @param currentplayer
	 *            the player who moves
	 * @param destination
	 *            the cell which goes the player
	 * 
	 */
	public void move (cell destination) {
		if (compareIndex(this.players[currentPlayer].getLocation(), destination) == true) {
			this.players[currentPlayer].getMoney(200);
		}

		this.players[currentPlayer].changeLocation(destination);
	}
	
	/**
	 * Finds a cell in cells array according to its name Signature:
	 * void_findCell_String Type:accessor
	 * 
	 * @param name
	 *            Cell name
	 */
	public cell findCell(String name) {
		int ctr;
		for (ctr = 0; ctr < this.cells.length; ctr++) {
			if (this.cells[ctr].getName().compareTo(name) == 0) {
				return this.cells[ctr];
			}
		}
		return null;
	}
	
	/**
	 * Gives turn to next player
	 * Signature:void_nextPlayer
	 * Type:transformer
	 */
	public void nextPlayer() {
            if (this.currentPlayer == this.players.length-1) {
                this.currentPlayer = 1;
            }
            else {
            	this.currentPlayer++;
            }
	}
	
	/**
	 * Returns an array of two random numbers [1,6]
	 * Signature: int[]_rollDice
	 * Type: accessor
	 * @return int[] array of 2 numbers [1,6]
	 */
	public int[] rollDice() {
		int[] nums = new int[2];
		nums[0] = gDice.rollDice();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println ("Kati egine");
        }
		nums[1] = gDice.rollDice();
                
		return nums;
	}
	
	/**
	 * Finds a cell according to its name
	 * Signature: cell_findCell_int
	 * @param steps Steps forward (+) or backwards (-) from current cell
	 * @return cell Cell found
	 */
	public cell findCell(int steps) {
		int ctr;
		for (ctr = 0; ctr <= this.cells.length; ctr++) {
			if (this.cells[ctr] == this.players[this.currentPlayer].getLocation()) {
					return this.cells[(ctr + steps) % this.cells.length]; 
			}
		}
		return null;
	}
	
	/**
	 * Checks if players owns block to which land belongs
	 * Signature: boolean_checkIfPlayerOwnsBlock_landPropertyCell
	 * Type:accessor
	 * @param land A cell to check if he owns the whole block
	 * @return true if owns, false if not
	 */
	private boolean checkIfPlayerOwnsBlock(landPropertyCell land) {
		int ctr;
		for (ctr = 0; ctr < this.cells.length; ctr++) {
			if (this.cells[ctr] instanceof landPropertyCell) {
				if (((landPropertyCell)(this.cells[ctr])).getColour().compareTo(land.getColour())== 0) {
					if (((landPropertyCell)(this.cells[ctr])).getOwner() != this.currentPlayer) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * Checks if player can build on a land cell
	 * Signature: boolean_checkIfCanBuildHere_landPropertyCell
	 * Type:accessor
	 * @param land land cell to check
	 * @return true if yes,false if not
	 */
	public boolean checkIfCanBuildHere(landPropertyCell land) {
		Vector blockBuildings = new Vector();
                boolean owns = this.checkIfPlayerOwnsBlock(land);
                int ctr;
                int level = 4;

                for (ctr = 0; ctr < this.cells.length; ctr++) {
                	if (this.cells[ctr] instanceof landPropertyCell &&((landPropertyCell)this.cells[ctr]).getColour().compareTo(land.getColour()) == 0) {
                		blockBuildings.add(this.cells[ctr]);
                	}
                }

                for (ctr = 0; ctr< blockBuildings.size(); ctr++) {
                        if (((landPropertyCell)blockBuildings.get(ctr)).getBuildings() == 5) return false;
                	if (((landPropertyCell)blockBuildings.get(ctr)).getBuildings() < level) {
                		level = ((landPropertyCell)blockBuildings.get(ctr)).getBuildings();
                	}
                }
                if (land.getBuildings() == level && owns == true) return true;
                return false;
	}
	
	/**
	 * Checks if can build hotel on land
	 * Signature: boolean_checkIfCanBuildHotelHere_landPropertyCell
	 * @param land cell to check
	 * @return true if yes,false if not
	 */
	public boolean checkIfCanBuildHotelHere(landPropertyCell land) {
		Vector blockBuildings = new Vector();
		int ctr;
		int buildings = 0;
		
		for (ctr = 0; ctr < this.cells.length; ctr++) {
			if (this.cells[ctr] instanceof landPropertyCell && ((landPropertyCell)this.cells[ctr]).getColour() == land.getColour()) {
				blockBuildings.add(this.cells[ctr]);
				buildings = buildings + ((landPropertyCell)(this.cells[ctr])).getBuildings();
			}
		}
		if (buildings ==(4 * blockBuildings.size())) return true;
		else return false;
	}

		/**
		 * Return cell aray to GUI for update
		 * Type:accessor
		 * Signature: cell[]_GUIreturnCells
		 * @return cell[] cell array
		 */
		public cell[] GUIreturnCells () {
			return this.cells;
       }
        
		/**
		 * Returns current player number to GUI
		 * Signature: int_GUIreturnCurrentPlayer()
		 * Type:accessor
		 * @return int currentPlayers number
		 */
        public int GUIreturnCurrentPlayer() {
            return this.currentPlayer;
        }
        
        /**
         * Returns current player's location for update
         * Signature: cell_GUIreturnPlayerLocation_int
         * Type:accessor
         * @param player player's to get location
         * @return cell location of player
         */
        public cell GUIreturnPlayerLocation(int player) {
            return this.players[player].getLocation();
        }
        
        /**
         * Returns player's credit 
         * Signature:int_GUIreturnPlayerCredit_int
         * Type:accessor
         * @param player player to get credit
         * @return int player's credit
         */
        public int GUIreturnPlayerCredit(int player){
            return this.players[player].getCredit();
        }
        
        /**
         * Returns a String array of properties names
         * Signature: String[]_GUIreturnPropertiesName_int
         * Type:accessor
         * @param player player to take property names from
         * @return String[] names of properties
         */
        public String[] GUIreturnPropertiesName (int player) {
            try {
                return this.players[player].GUIreturnPropertyTitlesNames();
            }
            catch (MonopolyException exe){
                // An den exei idioktisies, epistrefei enan array mono me to string "kamia idioktisia"
                // gia na fainetai sti lista titlon idioktisias
                String[] e = new String[1];
                e[0] = new String("kamia idioktisia");
                return e;
            }
        }
        
        /**
         * Returns player 1,2,3,4,5,6
         * Signature: player_GUIreturnPlayer_int
         * Type:accessor
         * @param p player to return
         * @return player player asked
         */
        public player GUIreturnPlayer(int p) {
            return this.players[p];
        }
        
        /**
         * Returns index of cell loc in cells[] array of GameMaster
         * Signature: int_GUIreturnCellIndex_cell
         * Type: accessor
         * @param loc cell to search index 
         * @return int cel's index
         */
        public int GUIreturnCellIndex (cell loc) {
		int ctr;
		for (ctr = 0;ctr<this.cells.length; ctr++) {
			if (this.cells[ctr] == loc) return ctr;
		}
		return -1;
	}
	
	/**
	 * The player buy property Signature:boolean_buyProperty_player
	 * Type:transformer
	 * 
	 * @param currentPlayer
	 *            the player who buyes
	 * @return true if he buyes,false if not preconditions:player must have
	 *         money to buy postconditions:the bank must gives to player the
	 *         property title
	 * @throws noMoneyException
	 * 
	 */
	public void buyProperty() throws noMoneyException {
                propertyCell t= (propertyCell)this.players[currentPlayer].getLocation();
		this.players[currentPlayer].giveMoneyToBank(t.getPrice());
		t.changeOwner(this.currentPlayer);
		this.players[currentPlayer].getTitle(this.trapeza.giveTitleToPlayer(t));
                t.makeUnAvailable();
    }
	
	/**
	 * The player build house at a cell Signature:boolean_buildHouse_player_cell
	 * Type:transformer
	 * 
	 * @param currentPlayer
	 *            the player who builds
	 * @param where
	 *            the cell where he builds
	 * @return true if he builds,false if not preconditions:houses smaller than
	 *         5 and player must own the same preconditions:colour cells
	 *         postconditions:the bank gives a house to player
	 * 
	 */
	public void buildHouse(landPropertyCell where) throws fullHouseException,noHousesException {
		if (this.checkIfPlayerOwnsBlock(where)==true && this.checkIfCanBuildHere(where) == true) {
			this.trapeza.giveHouseToPlayer();
			where.makeHouse();
			System.out.println(where.getName() + " "+where.getBuildings());
		}
	}
	
	/**
	 * Builds a house in a cell Signature: boolean_buildHotel_player_cell
	 * precondition: player has money to build, cell and all same coloured cells
	 * owned by him, cell.buildings = 4 postcondition: cell buildings = 5
	 * 
	 * @param currentPlayer
	 *            player to build
	 * @param where
	 *            cell to build at
	 * @return true if build done, false if not
	 * @throws noHotelsException 
	 * @throws hotelAlreadyException 
	 * @throws noHousesException 
	 * @throws noMoneyException 
	 */	
	public void buildHotel(landPropertyCell where) throws noHotelsException, hotelAlreadyException, noHousesException, noMoneyException {
            int ctr;
            int buildingsPrice = where.getBuildings() * where.getBuildingPrice();
            this.players[currentPlayer].getMoney(buildingsPrice);
            this.players[currentPlayer].giveMoneyToBank(where.getBuildingPrice());
            where.makeHotel();
	}
	
	/**
	 * Sells a property to another player Signature:
	 * boolean_sellProperty_player_player_cell precondition: players owns cell,
	 * buyer has enough money to buy postcondition: title Card transferred to
	 * buyer, cell owned by buyer
	 * 
         * 
	 * @param seller
	 *            player to sell
	 * @param buyer
	 *            player to buy
	 * @param property
	 *            cell to be sold
	 * @return true if property is morted, false if not
	 */
	public boolean sellProperty(player seller,player buyer,propertyCell property) throws noMoneyException{
            buyer.giveMoneyToPlayer(seller,property.getPrice());
            property.changeOwner(buyer.getSymbol());
            buyer.getTitle(seller.giveTitle(property.getName()));
            //Ypothiki
            return property.checkIfMort();
        }
	
	/**
	 * Sells property to bank Signature: boolean_sellProperty_player_cell
	 * precondition: player owns cell postcondition: title returned to bank
	 * 
	 * @param currentPlayer
	 *            player to sell
	 * @param property
	 *            Cell to sell
	 * @return true if done, false if not
	 */
	public void sellProperty(propertyCell property) throws noMoneyException{
            this.players[currentPlayer].giveMoneyToBank(property.getPrice());
            property.changeOwner(0);
            this.trapeza.takeTitle(this.players[currentPlayer].giveTitle(property.getName()));
        }

	/**
	 * Gets a loan from bank Signature: boolean_getLoan_player_int
	 * 
	 * @param currentPlayer
	 *            player to get loan
	 * @param money
	 *            money to loan
	 * @return true if done, false if not
	 */
	public void getLoan(int money){
        }

	/**
	 * Player goes to jail Signature: void_goToJail_player
	 * 
	 * @param currentPlayer
	 *            player to go to jail
	 */
	public void goToJail () {
		this.players[currentPlayer].goToJail();
		this.move(this.cells[10]);
	}

	/**
	 * Player gets out of jail 
	 * Signature: void_getOutOfJail_int 
	 * currentPlayer jailed 
	 * @param currentPlayer
	 *            player to get out of jail
	 */
	public void getOutOfJail(){
            this.players[currentPlayer].getOutOfJail();
        }
	
	/**
	 * Current player pays money to bank
	 * Signature: void_payMoneyToBank_int
	 * type:accessor
	 * @param money money to pay
	 * @throws noMoneyException
	 */
	public void payMoneyToBank(int money) throws noMoneyException {
		this.players[currentPlayer].giveMoneyToBank(money);
	}
	
	/**
	 * Current player takes money from bank
	 * Signature: void_takeMoneyFromBank_int
	 * type:transformer
	 * @param money money to take from bank
	 */
	public void takeMoneyFromBank(int money) {
		this.players[currentPlayer].getMoney(money);
	}
	
	/**
	 * Current player takes money from all players
	 * Signature:void_takeMoneyFromPlayer_int
	 * @param money money to take from each player
	 * @throws noMoneyException
	 */
	public void takeMoneyFromPlayers(int money) throws noMoneyException {
		int ctr;
		for (ctr = 0; ctr <= this.players.length; ctr++) {
			this.players[ctr].giveMoneyToPlayer(this.players[currentPlayer],money);
		}
	}
	
	public void giveMoneyToPlayers(int money) throws noMoneyException {
		int ctr;
		for (ctr = 0; ctr <= this.players.length; ctr++) {
			this.players[currentPlayer].giveMoneyToPlayer(this.players[ctr],money);
		}
	}
	
	/**
	 * Players pays for all buildings in game card
	 * 
	 * @throws noMoneyException
	 *             Signature: void_payForAllBuildings
	 */
	public void payForAllBuildings() throws noMoneyException {
		// 32 spitia 12 ksenodoxeia
		int houses;
		int hotels;
		houses = 32 - this.trapeza.getHouses();
		hotels = 12 - this.trapeza.getHotels();
		this.players[currentPlayer].giveMoneyToBank(houses*40 + hotels*115);
	}
	
        public void payRent() {
        }
        
	/**
	 * Player pays for his buildings Signature: void_payPlayerBuildings
	 * 
	 * @throws noMoneyException
	 */
	public void payPlayerBuildings() throws noMoneyException {
		int houses = 0;
		int hotels = 0;
		int ctr;

		for (ctr = 0; ctr <= this.cells.length;  ctr++) {
			if (this.cells[ctr].getOwner() == this.currentPlayer && this.cells[ctr] instanceof landPropertyCell) {
				if (((landPropertyCell) (this.cells[ctr])).getBuildings() == 5) {
					hotels++;
				} 
				else {
					houses++;
				}
			}
		}
		this.players[this.currentPlayer].giveMoneyToBank(houses*25 + hotels*100);
	}
        
        public void mortProperty(propertyCell land) {
            land.makeMort();
        }
        
        public void unMortProperty(propertyCell land) throws noMoneyException {
            this.getCurrentPlayer().giveMoneyToBank(land.getMortPrice() + ((int)0.01 * land.getMortPrice()));
            land.makeUnMort();
        }
}

	