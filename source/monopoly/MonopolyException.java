package monopoly;
public class MonopolyException extends Exception{

	private static final long serialVersionUID = 1L;

	public MonopolyException (String message) {
		super(message);
	}

}
