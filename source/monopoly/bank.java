package monopoly;
import java.util.Vector;

public class bank {

	private Vector properties;
	private int houses;
	private int hotels;
	
	/**
	 * Constructs a new bank
	 * Signature: bank
	 * Type:constuctor
	 * 
	 */
	public bank() {
                this.properties = new Vector();
		this.houses = 32;
		this.hotels = 12;
	}

	/**
	 * Returns number of hotels remaining
	 * Signature: int_getHotels
	 * @return int hotels remainingin bank
	 */
	public int getHotels() {
		return this.hotels;
	}
	
	/**
	 * Returns number of houses remaining
	 * 
	 * @return number of houses in bank
	 */
	public int getHouses() {
		return this.houses;
	}
	
        /**
         * Adds propertyTitle to vector
         * Signature: void_takeTitle_propertyTitle
         * Type: modifier
         * @param title propertyTitle to insert
         */
        public void takeTitle(propertyTitle title) {
            this.properties.addElement(title);
        }
        
	/**
	 * The bank gives money to player and increases its credit
	 * Signature:void_giveMoneyToPlayer_int_player
	 * Type:transformer
	 * @param money the money gets the player
	 * @param geter the player who get the money
	 * 
	 */
	public void giveMoneyToPlayer(int money, player geter) {
		geter.getMoney(money);
	}
	
	/**
	 * Decreases bank number of houses by 1
	 * Signature:void_ giveHouseToPlayer_player
	 * Type:transformer
	 * @param buyer the player who buy the house
	 * preconditions: houses >  0
	 * postconditions: houses = houses - 1;
	 * @throws noHousesException 
	 * 
	 */
	public void giveHouseToPlayer() throws noHousesException {
		if (this.houses > 0) {
			this.houses--;
			
		}
		else {
			throw new noHousesException();
		}

	}
	
	/**
	 * Decreases bank number of hotels
	 * Signature:void_ giveHotelToPlayer_player
	 * Type:transformer
	 * @param buyer the player who buy the hotel
	 * @return true if bank has houses and gave, false if no more houses in bank
	 * preconditions:hotels > 0
	 * postconditions:hotels = hotels - 1
	 * @throws noHotelsException 
	 *
	 */
	public void giveHotelToPlayer () throws noHotelsException {
		 if (hotels > 0) {
			 this.hotels--;
		 }
		 else {
			 throw new noHotelsException(); 
		 }
			
	 }
	
	/**
	 * Gives to player the property title
	 * Signature:void_giveTitleToPlayer_player
	 * Type:transformer
	 * @param buyer	the player who buy it
	 * preconditions:the player must express a will to buy it
	 * postconditions:the player has it
	 * 
	 */
	public propertyTitle giveTitleToPlayer(propertyCell land) {
            int ctr;
            for (ctr = 0; ctr < this.properties.size(); ctr++) {
    		if (((propertyTitle)properties.get(ctr)).getLand() == land) return ((propertyTitle)properties.remove(ctr));
            }
            System.out.println ("o titlos idioktisias de vretike");
            return null;
	}
	
	/**
	 * The gives a loan to player
	 * Signature:void_giveLoanToPlayer_int_player
	 * Type:transformer
	 * @param loaner the player who loan
	 * preconditions:the player must ask for loan
	 * postconditions:the player owns to bank
	 * 
	 */
	public void giveLoanToPlayer(int money, player loaner) {
		this.giveMoneyToPlayer(money,loaner);
	}
	
	/**
	 * The bank gets money from player
	 * Signature:void_getMoneyFromPlayer_int_player
	 * Type:transformer
	 * @param money the cash which take from player
	 * @param giver the player who gives money
	 * preconditions:the player must has the money or property
	 * postconditions:the player's money decreased
	 * 
	 */
	public void getMoneyFromPlayer(int money,player giver) {
		try {
			giver.giveMoneyToBank(money);
		}
		catch (noMoneyException e) {
                       // e.showErrorDialog();
		}
	}
	
	/**
	 * The bank increases its number of houses
	 * Signature:void_getHouseFromPlayer_player
	 * Type:transformer
	 * @param seller the player who sell the house
	 * 
	 */
	public void makeHouse() {
		this.houses++;
	}
	
	/**
	 * The bank increases its number of hotels
	 * Signature:void_getHotelFromPlayer_player
	 * Type:transformer
	 * @param seller the player who sell the hotel
	 *
	 */
	public void makeHotel() {
		this.hotels++;
	}
		

}
