package monopoly;

public class bankMoneyCard extends moneyCard
{
	
	/**
	 * Constructs a new bankMoneyCard
	 * Signature:bankMoneyCard_String_int
	 * Type:Constructor
	 * @param message the message of bankMoneyCard
	 * @param money the money to be payed to bank
	 * @param takeMoney true if player takes money, false if not
	 */
	public bankMoneyCard(String message, int money,boolean takeMoney) {
		super(message,money,takeMoney);
	}
	
	/**
	 * Execute action
	 * Signature:voiud_execute
	 * Type:transformer
	 * @throws noMoneyException 
	 */
	public void execute(GameMaster a) throws noMoneyException {
		if (this.getTake() == true) {
			a.takeMoneyFromBank(this.getMoney());
		}
		else {
			a.payMoneyToBank(this.getMoney());
		}
	}
}
