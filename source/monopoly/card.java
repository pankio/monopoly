package monopoly;

public abstract class card {
	private String message;

	/**
	 * Constructs a new card
	 * Signature: card_String
	 * Type: constructor
	 * @param message The message of card
	 */
	public card (String message) {
		this.message = message;
	}

	/**
	 * Returns card message;
	 * Signature: String_getMessage
	 * @return string Card message
	 */
	public String getMessage() {
		return this.message;
	}
	
	/**
	 * Executes card action
	 * Signature: void_execute
	 * Type: accessor
	 * @throws noMoneyException 
	 */
	abstract public void execute(GameMaster a) throws noMoneyException;
}
