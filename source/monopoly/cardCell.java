package monopoly;

public class cardCell extends cell{
	private boolean commandCell;	

	/**
	 * Constructs a new cardCell
	 * Signature: cardCell_String_boolean
	 * Type: constructor
	 */
	public cardCell(String name,boolean commandCell) {
		super(name,false);
		this.commandCell = commandCell;
	}
	
	/**
	 * Checks if cell is command cell or not
	 * Signature: boolean_checkCommand
	 * Type: accessor
	 * @return true if command cell, false if decision cell
	 */
	public boolean checkCommand() {
		return this.commandCell;
	}
	
	/**
	 * Execute cell action
	 * Signature: void_execute
	 * Type: accessor
	 */
	public boolean[] execute(GameMaster a) throws MonopolyException {
		boolean en[] = new boolean[6];
		en[0] = false;
		en[1] = false;
		en[2] = false;
		en[3] = true;
		en[4] = true;
		return en;
	}
}
