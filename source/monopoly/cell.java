package monopoly;

/**
 * @author  Administrator
 */
abstract  public class cell {
	private String name;
	private int owner;
	private boolean available;

	/**
	 * Constructs a new instance of cell
	 * Signature:cell_String_boolean
	* Type:Constructor
	* @param	name Cell name
	* @param	available true when available, false when unavailable
	*/
	public cell(String name,boolean available) {
		this.name = name;
		this.available = available;
		this.owner = 0;
	}

	/**
	 * Executes cell action
	 * Signature:void_execute_GameMaster
	 * Type:accessor
	 * @param a the GameMaster that masters the game
	 * @throws MonopolyException 
	 */		
	public abstract boolean[] execute(GameMaster a) throws MonopolyException;

	/**
	 * Returns cell name
	 * Signature:String_getName Type: accessor
	 * @return  String The name of the cell
	 * @uml.property  name="name"
	 */

	public String getName() {
		return this.name;
	}
	       
	/**
	 * Signature:int_getOwner Type:accessor
	 * @return  int Owner of the cell
	 * @uml.property  name="owner"
	 */

	public int getOwner() {
		return this.owner;
	}
	
	/**
	 * Changes the owner of the cell
	 * Signature:void_changeOwner_int
	 * Type: transformer
	 * @param owner The new owner of the cell
	 */
	public void changeOwner(int owner) {
		this.owner = owner;
	}
	
	/** 
	 * Checks if cell is available
	 * Signature:boolean_checkIfAvailable
	 * Type: accessor
	 * @return	true if available, false otherwise
	 */
	public boolean checkIfAvailable() {
		return this.available;
	}
	
	/** 
	 * Makes cell available
	 * Signature:void_makeAvailable
	 * Type:transformer
	 */
	public void makeAvailable() {
		this.available = true;
	}
	
	/**
	 * Makes cell unavailable
	 * Signature:void_makeUnAvailable
	 * Type:transformer
	 */
	public void makeUnAvailable() {
		this.available = false;
	}
}
