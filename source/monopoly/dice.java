package monopoly;
import java.util.Date;
import java.util.Random;

public class dice {
    
    /**
     * Returns int from 1 to 6
     * Signature: int_rollDice
     * Type: accessor
     *
     * @return int the number bring the dice
     */
    public int rollDice() {
        int NumToGuess;
        Date curDate = new Date();
        
        Random RandomGenerator = new Random((curDate.getTime()));
        NumToGuess=RandomGenerator.nextInt(5) + 1;
        System.out.println(NumToGuess);
        return NumToGuess;
    }
}
