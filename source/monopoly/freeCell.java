package monopoly;

public class freeCell extends cell{
	
	/**
	 *Constructs a new free cell
	 *Signature:freeCel_String
	 *Type: Constructor
	 */
	public freeCell(String name) {
		super (name,false);
	}
	
	/**
	 *Executes action
	 *Signature:void_execute
	 *Type:accessor
	 */
	public boolean[] execute(GameMaster a) {
		boolean en[] = new boolean[5];
		if (this.getName().compareTo("FYLAKH")==0) {
			if (a.getCurrentPlayer().hasJailCard()==true) en[1]=true;
			else en[1] = false;
		}
		else en[1] = false;
		en[2] = false;
		en[3] = false;
		en[4] = true;
		return en;
	}
	
}
