package monopoly;

public class goToJailCell extends cell{
	
	/**
	 *Constructs a new go to jail cell
	 *Signature:goToJailCell_String
	 *Type:constructor
	 */
	public goToJailCell() {
		super("Go to jail",false);
	}

	/**
	 *Execute action, send current player to jail
	 *Signature: void_execute
	 *Type: transormer
	 */
	public boolean[] execute(GameMaster a) {
		boolean[]  en= {false,false,false,false,false};
		a.goToJail();
		return en;
	}
}
