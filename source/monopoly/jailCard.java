package monopoly;

public class jailCard extends card 
{
	private boolean goToJail;
	
	/**
	 * Constructs a new jail card
	 * Signature:jailCard_String_boolean
	 * Type:Constructor
	 * @param message the message of the card
	 * @param goToJail
	 */
	public jailCard (String message, boolean goToJail) {
		super(message);
		this.goToJail = goToJail;
	}
	
	/**
	 * Executes action
	 * Signature:void_execute
	 * Type:transformer
	 */
	public void execute(GameMaster a) {
		if (this.goToJail == true) {
			a.goToJail();
		}
		else {
			a.getOutOfJail();
		}
	}

}
