package monopoly;
/**
 * @author  Administrator
 */
public class landPropertyCell extends propertyCell {
	private int rent;
	private String colour;
	private int buildings;
	private int buildingPrice;

	
	/**
	 * Constructs a new instance of cell
	 * Signature:properyCell_String_String_int_int
	 * Type:Constructor
	 * @param	name Cell name
	 * @param   colour cell colour
	 * @param   price cell price
	 * @param   rent property rent
	 * @param  	buildingPrice buildingPrice
	 * @param 	mortPrice mortPrice
	 */
	public landPropertyCell (String name, String colour,int price, int rent,int buildingPrice/*, int mortPrice*/) {
		super(name, true,price);
		this.rent = rent;
		this.colour = colour;
		this.buildings = 0;
		this.buildingPrice = buildingPrice;
//		this.mortPrice = mortPrice;
	}

	/**
	 * Changes rent
	 * Signature: void_changeRent
	 * Type: transformet
	 */
	public void changeRent() {
		this.rent = this.rent * this.buildings;
	}
	
	/**
	 * returns building price of cell
	 * signature: int_getBuildingPrice
	 * @return int building price
	 */
	public int getBuildingPrice() {
		return this.buildingPrice;
	}
	
	/**
	 * Returns rent price of property cell
	 * @return int property cell rent price
	 * Signature: int_getRent
	 * Type: accessor
	 */
	public int getRent() {
		return this.rent;
	}


	
	/**
	 * Increases cells number of houses
	 * signature: void_makeHouse
	 * @throws fullHouseException
	 */
	public void makeHouse() throws  fullHouseException {
		if (this.buildings < 4) this.buildings++;
		else throw new fullHouseException();
	}
	
	/**
	 * Decreases cell number of houses
	 * signature: void_removeHouse
	 * @throws noHousesException
	 */
	public void removeHouse() throws noHousesException {
		if (this.buildings > 0 && this.buildings < 5) buildings--;
		else throw new noHousesException();
	}
	
	/**
	 * Makes hotel in cell
	 * Signature: void_makeHotel
	 * @throws hotelAlreadyException
	 */
	public void makeHotel() throws hotelAlreadyException {
		if (this.buildings <=4 ) this.buildings = 5;
		else throw new hotelAlreadyException();
	}
	
	/**
	 * removes hotel from cell
	 * void_removeHotel
	 * @throws noHotelsException
	 */
	public void removeHotel() throws noHotelsException {
		if (this.buildings == 5) this.buildings = 0;
		else throw new noHotelsException();
	}
	
    /**
	 * Returns the colour of the property Signature:String_getColour Type:accessor
	 * @return  String the colour of property
	 * @uml.property  name="colour"
	 */
	public String getColour() {
		return this.colour;
	}
	
	/**
	 * Executes cell action
	 * Signature:void_execute
	 * Type: transformer
	 * 
	 */		
	 public boolean[] execute(GameMaster a) throws noMoneyException {
			boolean en[] = new boolean[6];
			if (this.checkIfAvailable() == true)  en[0] = true;
			else en[0] = false;
			en[1] = false;
			en[2] = false;
			en[3] = false;
			en[4] = true;
                        if (this.getOwner() != 0 && this.getOwner()!= a.getCurrentPlayer().getSymbol()) {
                        	a.getCurrentPlayer().giveMoneyToPlayer(a.GUIreturnPlayer(this.getOwner()),this.getRent());
                        }
			return en;
	 }

	/**
	 * Signature: int_getBuildings
	 * Type: accessor
	 * @return int number of buildings if 5 cell has a hotel and no houses
	 */
	public int getBuildings() {
		return this.buildings;
	}

}
