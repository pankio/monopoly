package monopoly;

public abstract class moneyCard extends card{
	private int money;
	private boolean takeMoney;
	
	/**
	 * Constructs a new moneyCard
	 * Signature:moneyCard_String_int
	 * Type: Constructor
	 * 
	 * @param message the message of money card
	 * @param money the money to be payed
	 * @param takeMoney true if player gets money, false if not
	 */
	public moneyCard(String message,int money,boolean takeMoney) {
		super(message);
		this.money = money;
		this.takeMoney = takeMoney;
	}

	/**
	 * 
	 * @return
	 */
	public int getMoney() {
		return this.money;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getTake() {
		return this.takeMoney;
	}
	
}	

