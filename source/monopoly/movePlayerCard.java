package monopoly;

public abstract class movePlayerCard extends card{
	
	/**
	 * Constructs a new movePlayerCArd
	 * Signature: movePlayerCard_String
	 * Type: accessor
	 */	
	public movePlayerCard(String message) {
		super(message);
	}

	/**
	 * Executes card action
	 * Signature: void_execute
	 * Type: accessor
	 */
	abstract public void execute(GameMaster a);
	
}
