package monopoly;

public class moveStepsCard extends movePlayerCard{

	private int steps;
	
	/**
	 * Constructs a new moveStepsCard
	 * Signature:moveStepsCard_String_int
	 * Type: Constructor
	 * @param message the message of the moveStepsCard
	 * @param steps the steps to be moved the player
	 *  
	 */
	public moveStepsCard(String message,int steps) {
		super(message);
		this.steps = steps;
	}
	
	/**
	 * Execute action
	 *Signature:void_execute
	 *Type:transformer
	 *
	 */
	public void execute(GameMaster a) {
		a.move(a.findCell(this.steps));
	}
}



