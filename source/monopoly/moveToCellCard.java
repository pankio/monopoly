package monopoly;

/**
 * @author  Administrator
 */
public class moveToCellCard extends movePlayerCard{
	private cell newlocation;
	
	/**
	 * Constructs a new moveToCellCard 
	 * Signature: moveToCellCard_String_cell
	 * Type:Constructor
	 * @param message the message of the card
	 * @param newlocation the new location of player 
	 */
	public moveToCellCard(String message, cell newlocation) {
		super(message);
		this.newlocation = newlocation;
	}
	
	/**
	 * Execute action
	 * Signature:void_execute
	 * Type:transformer
	 *
	 */
	public void execute(GameMaster a) {
		a.move(this.newlocation);
	}
}
