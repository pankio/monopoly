package monopoly;

public class moveToClosestRailRoadCard extends movePlayerCard
{	
	
	/**
	 * Constructs a new moveToClosestRailRoad card
	 * Signature:moveToClosestRailRaodCard_String
	 * Type:Constructor
	 * @param message the message of the card
	 */
	public moveToClosestRailRoadCard(String message) {
		super(message);
	}
	
	/**
	 * Find the nearest RailRoad cell
	 * Siganture:cell_checkClosestRailRoad
	 * Type:accessor
	 * 
	 * @return cell the closest RailRoad cell
	 */
	private cell checkClosestRailRoad(GameMaster a) {
		return	a.findCell("Sidirodromikos stathmos");
	}
	
	/**
	 * Execute action
	 * Signature:void_execute
	 * Type:transformer
	 */
	public void execute(GameMaster a) {
		a.move(this.checkClosestRailRoad(a));
	}
}
