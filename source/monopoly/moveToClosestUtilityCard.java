package monopoly;

public class moveToClosestUtilityCard extends movePlayerCard
{

	/**
	 * Constucts a new moveToClosestUtilityCard
	 * Signature:moveToClosestUtilityCard_String
	 * Type: Constuctor
	 * @param message the message of the moveToClosestUtilityCard
	 */
	public moveToClosestUtilityCard(String message) {
		super(message);
	}
	
	/**
	 * Finds the closest utility cell
	 *Signature:cell_checkClosestUtility
	 *Type:accessor
	 *
	 *@return cell the closest utility cell
	 */
	private cell checkClosestUtility(GameMaster a) {
		return a.findCell("Epixeirisi");
	}
	
	/**
	 * Execute action
	 * Signature:void_execute
	 * Type:transformer
	 * 
	 */
	public void execute(GameMaster a) {
		a.move(this.checkClosestUtility(a));
	}
}
