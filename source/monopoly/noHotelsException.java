package monopoly;

public class noHotelsException extends MonopolyException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public noHotelsException () {
		super("No more hotels to sell");
	}
}
