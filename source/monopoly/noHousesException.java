package monopoly;

public class noHousesException extends MonopolyException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public noHousesException() {
		super("No more houses in bank to sell");
	}
}
