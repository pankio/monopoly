package monopoly;

public class noMoneyException extends MonopolyException {

	private static final long serialVersionUID = 1L;

	public noMoneyException() {
		super("Player does not have enough money!");
	}
}
