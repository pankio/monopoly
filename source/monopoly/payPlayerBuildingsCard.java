package monopoly;
public class payPlayerBuildingsCard extends moneyCard {

	/**
	 * Constructs a new payPlayerBuildingsCard Signature: payPlayerBuildingsCard
	 * Type: Constructor
	 * 
	 * @param message
	 *            the message of the card
	 * 
	 */
	public payPlayerBuildingsCard(String message) {
		super(message, 0, false);
	}

	/**
	 * Execute action of card Signature: void_execute_GameMaster Type:
	 * transformer
	 * @throws noMoneyException 
	 * 
	 */
	public void execute(GameMaster a) throws noMoneyException {
		a.payPlayerBuildings();
	}
		
}