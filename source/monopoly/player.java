package monopoly;
import java.util.Vector;

/**
 * @author  Administrator
 */
public class player
{
	private int credit;
	private int symbol;
	private  cell location;
	private Vector titles;
	private jailCard jail;
	private boolean jailed;
	private int numberOfThrows;
        public int threeDoubles;
	/**
	 * Constructs a new player
	 * Signature: player_int
	 * Type: constructor
	 * @param credit Start credit
	 * @param symbol Player symbol
	 */
	public player( int symbol) {
		this.credit = 1500;
		this.symbol = symbol;
                this.titles = new Vector();
                this.jail = null;
                this.jailed = false;
                this.threeDoubles = 0;
        
	}

        public int getSymbol() {
            return this.symbol;
        }
        
	/**
	 * Returns player credit
	 * @return int player's credit
	 */
	public int getCredit() {
		return this.credit;
	}
	
	public String[] GUIreturnPropertyTitlesNames() throws MonopolyException {
            if (this.titles.size() == 0) throw new MonopolyException("Den exei titloys idioktisias");
            String[] names = new String[titles.size()];
            int ctr;
            for (ctr = 0; ctr < titles.size(); ctr++) {
                if (((propertyTitle)titles.get(ctr)).getLand().checkIfMort() == true) {
                    names[ctr] = ((propertyTitle)titles.get(ctr)).getName() + "(YPOTHIKI)";                    
                }
                else {
                    names[ctr] = ((propertyTitle)titles.get(ctr)).getName();
                }
            }
            return names;
	}
	
        /**
         * Returns true if jailed,false if not
         * Signature: boolean_isJailed
         * Type: accessor
         * @return true if jailed, false if not
         */
        public boolean isJailed() {
            return this.jailed;
        }
        
	/**
	 * Signature: cell_getLocation
	 * Type: accessor
	 * @return location the location of player
	 */
	public cell getLocation() {
		return this.location;
	}
	
	/**
	 * Signature: void_changeLocation_cell
	 * Type: accessor
	 * @param newLocation
	 */
	public void changeLocation(cell newLocation) {
		this.location = newLocation;
	}
       
	/**
	 * Changes number of dice throws
	 * Signature: void_changeNumberOfThrows_int[]
	 * Type: transformer
	 * @param a An array of dice throw results
	 */
	public void changeNumberOfThrows (int[] a){}

	/**
	 * Increases player credit
	 * Signature: void_getMoney_int
	 * Type: transformer
	 * @param money money to get
	 */
	public void getMoney(int money) {
		this.credit+= money;
	}
	
	/**
	 * Gives money to bank from player credit, decreasing its credit
	 * Signature: void_giveMoneyToBank_int
	 * Type: transformer
	 * precondition: credit >= money
	 * postcondition: credit- = money, bank.credit+ = money
	 * @param money money to give to bank
	 * @throws noMoneyException if has no money
	 */
	public void giveMoneyToBank (int money) throws noMoneyException {
		if (credit >= money) {
			credit-= money;
		} 
		else {
			throw new noMoneyException();
		}
	}

	/**
	 * Gives money to another player from player credit and increases a's credit
	 * Signature: void_giveMoneyToPlayer_int
	 * Type: transformer
	 * precondition: credit >= money
	 * postcondition: credit- = money, a.credit+ = money
	 * @param money money to give to player
	 * @throws noMoneyException  if no money
	 */
	public void giveMoneyToPlayer(player a,int money) throws noMoneyException {
		if (credit >= money) {
			this.credit-=money;
			a.getMoney(money);
		}
		else {
			throw new noMoneyException();
		}
	}

	/**
	 * Makes jailed= false
	 * Signature: void_getOutOfJail
	 * Type: transformer
	 */
	public void getOutOfJail() {
		this.jailed = false;
	}

	/**
	 * Makes jailed= true
	 * Signature: void_getOutOfJail
	 * Type: transformer
	 */
	public void goToJail() {
		this.jailed = true;
	}
	
	/**
	 * Sells jailCard to another player
	 * Signature: jailCard_sellJailCard
	 * Type: transformer
	 * @param buyer player to buy jail card
	 * precondition: player has jail card (jailCard != null)
	 * postcondition: jailCard == null, credit increased
	 */
	public jailCard giveJailCard () {
		jailCard temp = this.jail;
		this.jail = null;
		return temp;
	}
	
	/**
	 * Buys jailCard from seller
	 * Signature: void_buyJailCard_player
	 * Type: transformer
	 * @param seller Seller to buy jail card from
	 * precondition: seller has jailCard, player enough money to buy
	 * post: seller has not jail card, player does
	 */ 
	public void takeJailCard (player seller) {
		this.jail = seller.giveJailCard();
	
	}
	
	public void getJailCard(jailCard a) {
		this.jail = a;
	}
	
	public boolean hasJailCard() {
		if (this.jail == null) return false;
		return true;
	}

	/**
	 * Returns 2 integers of a dice roll
	 * Signature: int[]_throwDice
	 * @return int[] 2 random integers [1,6]
	 */
	public int[] throwDice(){int a[] = new int[2];a[0]=1;a[1]=1;return a;}

	/**
 	 * Gets a property title 
	 * Signature: void_getTitle_title
	 * Type: transformer
	 * @param a a property titlte 
	 * precondition: nothing
	 * postcondition: a added in title array
	 */ 
	public void getTitle(propertyTitle a) {
		this.titles.addElement(a);
	}
		
	public propertyTitle giveTitle(String name) {
            int ctr;
            for (ctr = 0;ctr<=this.titles.size();ctr++) {
                if (((propertyTitle)this.titles.get(ctr)).getName().compareTo(name) == 0) 
                    return (propertyTitle)this.titles.remove(ctr);
            }
            return null;
        }
        
	/**
	 * Finds a propertyTitle according to its name
	 * Signature: cell_findProperty_String
	 * Type: accessor
	 * @param name Name of property title
	 * @return cell Property cell that corresponds to title 
	 */
	public propertyTitle findProperty(String name) {
		int ctr;
		for (ctr = 0; ctr< titles.size(); ctr++) {
			if ( ((propertyTitle)this.titles.get(ctr)).getName().compareTo(name) == 0) 
                            return (propertyTitle)this.titles.get(ctr);
        }
		return null;
	}
}