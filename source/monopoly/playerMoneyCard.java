package monopoly;

public class playerMoneyCard extends moneyCard
{
	
	/**
	 * Constructs a new playerMoneyCard
	 * Signature:playerMoneyCard_String_int
	 * Type:Constructor
	 * @param message the message of playerMoneyCard
	 * @param money to be payed to the player
	 * 
	 */
	public playerMoneyCard(String message, int money,boolean takeMoney) {
		super(message,money,takeMoney);
	}
	
	/**
	 * Execute action
	 * Signature:voiud_execute
	 * Type:transformer
	 * @throws noMoneyException 
	 */
	public void execute(GameMaster a) throws noMoneyException {
		if (this.getTake() == true) {
			a.takeMoneyFromPlayers(this.getMoney());
		}
		else {
			a.giveMoneyToPlayers(this.getMoney());
		}
	}
}