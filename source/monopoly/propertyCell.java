package monopoly;

public abstract class propertyCell extends cell {
	private int price;
	private boolean mort;
	private int mortPrice;
	
	public propertyCell(String name, boolean available,int price) {
		super(name, available);
		this.mort=false;
		this.price = price;
                this.mort=false;
                this.mortPrice = price;
	}
	
	/**
	 * Returns utility price 
	 * Signature: int_getPrice 
	 * Type: accessor
	 * @return  int Utility price
	 * @uml.property  name="price"
	 */
	public int getPrice() {
		return this.price;
	}

	/**
	 * Checks if utility is morted
	 * Signature: boolean_checkIfMort;
	 * Type: accessor
	 * @return true if morted, false if not
	 */
	public boolean checkIfMort() {
		return this.mort;
	}
	
	/** 
	 * Makes utility morted
	 * Signature: void_makeMort
	 * Type transformer
	 */
	public void makeMort() {
		this.mort = true;
	}

	/**
	 * Makes utility unmorted
	 * Signature: void_makeUnMort
	 * Type: transformer
	 */
	public void makeUnMort() {
		this.mort = false;
	}

	/**
	 * Returns mort price Signature: int_getMortPrice Type: accessor
	 * @return  int Mort price of utility
	 * @uml.property  name="mortPrice"
	 */
	public int getMortPrice() {
		return this.mortPrice;
	}
	
	public abstract boolean[] execute(GameMaster a) throws MonopolyException; 
}
