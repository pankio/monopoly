package monopoly;

/**
 * @author  Administrator
 */
public class propertyTitle
{
	private String name;
	private propertyCell land;
	
	/**
	 * Constructs a new propertyTitle
	 * Signature:propertyTitle_String_int_int
	 * Type:Constructor
	 * @param name the name of propertyTitle
	 * @param price the price of propertyTitle
	 * @param rent the rent of propertyTitle
	 */
	public propertyTitle(String name, propertyCell land) {
		this.name = name;
		this.land= land;
	}
	
	/**
	 * Gets the name of propertyTitle Signature:String_getName Type:accessor
	 * @return  String the name ofpropertyTitle
	 * @uml.property  name="name"
	 */
	public String getName() {
		return this.name;
	}
	
	public propertyCell getLand() {
		return this.land;
	}
}	


