package monopoly;

/**
 * @author  Administrator
 */
public class railRoadCell extends propertyCell{
	private int rent;


	/**
	 * Constructs a new rail road cell
	 * Signature: railRoadCel_String_int_int_int
	 * Type: constructor
	 * @param name cell name
	 * @param price cell price
	 * @param rent cell rent
	 * @param mortprice price to mort
	 */
	public railRoadCell(String name, int price, int rent/*, int mortPrice*/) {
		super(name,true,price);
		this.rent = rent;
	//	this.mortPrice = mortPrice;
	}
	
	/**
	 * Executes cell action
	 * Signature: void_execute
	 * Type: transformer
	 */
	public boolean[] execute(GameMaster a) throws noMoneyException {
		boolean en[] = new boolean[6];
		if (this.checkIfAvailable()==true) en[0] = true;
		else en[0]=false;
		en[1] = false;
		en[2] = false;
		en[3] = false;
		en[4] = true;
                if (this.getOwner() != 0 && this.getOwner()!=a.getCurrentPlayer().getSymbol()) a.getCurrentPlayer().giveMoneyToBank(this.getRent());
		return en;
	}
	/**
	 * Returns rent price of railroad cell Signature: int_getRent Type: accessor
	 * @return  int rent price of railroad cell
	 */
	public int getRent() {
		return this.rent;
	}
	

}
