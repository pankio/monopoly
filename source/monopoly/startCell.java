package monopoly;

public  class startCell extends cell{
	
	/**
	 *Constructs a new start cell
	 *Signature:startCell_String	
	 *Type: Constructor	
	 */
	public startCell() {
		super ("Afetiria",false);
	}
	
	/**
	 *Execute action
	 *Signature: void_execute
	 *Tyoe: transformer
	 */
	public boolean[] execute(GameMaster a) {
		boolean en[] = new boolean[6];
		en[0] = false;
		en[1] = false;
		en[2] = false;
		en[3] = false;
		en[4] = true;
		a.takeMoneyFromBank(150);
		return en;	
	}
}
