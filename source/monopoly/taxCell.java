package monopoly;

/**
 * @author  Administrator
 */
public class taxCell extends cell{
	private int tax;

	/**
	 *Constructs a new tax cell
	 *Signature: taxCell_String_int
	 *Type: Constructor
	 */
	public taxCell(String name,int tax) {
		super(name,false);
		this.tax = tax;
		
	}

	/**
	 * Gets the tax to be payed from player 
         * Signature:int_getTax Type:accessor
	 * @return  int the tax
	 * @uml.property  name="tax"
	 */
	public int getTax() {
		return this.tax;
	}

	/**
	 *Execute action
	 *Signature: void_execute
	 *Type: tansformer
	 * @throws noMoneyException 
	 */	
	public boolean[] execute(GameMaster a) throws noMoneyException {
		boolean[] en = new boolean[5];
		en[0] = false;
		en[1] = false;
		en[2] = false;
		en[3] = false;
		en[4] = true;
		a.payMoneyToBank(this.tax);
		return en;
	}
}
