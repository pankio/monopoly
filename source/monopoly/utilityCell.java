package monopoly;

/**
 * @author  Administrator
 */
public class utilityCell extends propertyCell{

	/**
	 * Constructs a new utility cell
	 * Signature: utilityCell_String_int
	 * Type: constructor
	 * @param name cell name 
	 * @param price   utility price
 	*/
	public utilityCell(String name,int price) {
		super(name, true,price);
	}
	
	/**
	 * Executes ultility cell action, asks player if wants to buy
	 * Signature: void_execute
	 * Type: transformer
	 * @throws noMoneyException 
	 */
	public boolean[] execute(GameMaster a) throws noMoneyException {
		boolean en[] = new boolean[6];
		if (this.checkIfAvailable()==true) en[0] = true;
		else en[0] = false;
		en[1] = false;
		en[2] = false;
		en[3] = false;
		en[4] = true;
		return en;
	}
	
	
}
